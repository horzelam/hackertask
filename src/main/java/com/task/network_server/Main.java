package com.task.network_server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class Main {
    public static void main(String[] args) throws IOException {
        // https://www.codejava.net/java-se/networking/java-socket-server-examples-tcp-ip
        ServerSocket serverSocket = new ServerSocket(6868);
        System.out.println("Started single-threaded server: " + serverSocket.getChannel());

        final Socket socket = serverSocket.accept();
        System.out.println("Accepted: " + socket.getRemoteSocketAddress() + " - " + socket.getPort());
        InputStream input = socket.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(input));
        while (true) {

            String line = reader.readLine();    // reads a line of text
            System.out.println("Read line: " + line);
            if (line != null && line.equals("exit")) {
                break;
            }
        }
        System.out.println("terminated");
    }
}
