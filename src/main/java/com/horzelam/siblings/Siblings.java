package com.horzelam.siblings;

import java.util.Arrays;

public class Siblings {

    public static void main(String[] args) {
        System.out.println(solution(213));
        System.out.println(solution(553));
        System.out.println(solution(1));
        System.out.println(solution(10));
        System.out.println(solution(66));
        System.out.println(solution(100));
        System.out.println(solution(1230009));
        System.out.println(solution(99999999));
        System.out.println(solution(100000000));
        // out of range
        System.out.println(solution(100000001));
        System.out.println(solution(-1));

    }

    public static int solution(int N) {

        if (N < 0) {
            return 0;
        } else if (N > 100000000) {
            return -1;
        } else if (N == 100000000) {
            return N;
        } else {

            final int maxAnalyzedDigits = 8;
            byte[] digits = new byte[maxAnalyzedDigits];
            // extract digits

            int number = N;
            int digitsIndex = 0;
            for (; digitsIndex < maxAnalyzedDigits && number > 0; digitsIndex++) {
                digits[digitsIndex] = (byte) (number % 10);
                number = number / 10;
            }

            final byte[] sorted = Arrays.copyOf(digits, digitsIndex);
            Arrays.sort(sorted);

            int result = 0;
            for (int i = sorted.length - 1; i >= 0; i--) {
                result = result * 10 + sorted[i];
            }
            // for debug:
            //debug(sorted);
            return result;

        }
    }

    private static void debug(final byte[] sorted) {
        StringBuilder resString = new StringBuilder();
        for (int i = sorted.length - 1; i >= 0; i--) {
            resString.append(String.valueOf(sorted[i]));
            if (i > 0) {
                resString.append(",");
            }
        }
        System.out.println(resString.toString());
    }
}
