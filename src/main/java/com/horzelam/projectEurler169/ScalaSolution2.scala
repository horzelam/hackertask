package com.horzelam.projectEurler169

/**
  * Created by osboxes on 07/01/17.
  */
object ScalaSolution2 {





    // 1010 = 10 = 8 + 2
    // ---------
    // ---------
    // na ile sposobow mozna poprzesuwac w prawo 1-ki
    // ---------
    // 1010 = 1000 + (0010)= 8 + 2 (rozlozenie na czynniki 1-sze) - (ostatni czynnik v1 - oruginalna kombinacja)
    // 0100 + 0100 + (0010) = 4+4+2 (ostatni czynnik v1 z wsztstkimi sensownymi kombinacjami innych)
    // ...
    // 1000 + (00001+ 0001) = 4+4+1+1 (ostatni czynnik v2 przesuniety o 1  z wsztstkimi sensownymi kombinacjami innych)
    // 0100 + 0100 + (0001 + 0001) = 4+4+1+1 (ostatni czynnik v2  przesuniety o 1  z wsztstkimi sensownymi kombinacjami innych)
    // 0100 + 0010 + 0010 + (0001 + 0001) = 4+2+2+1+1 (ostatni czynnik v2  przesuniety o 1  z wsztstkimi sensownymi kombinacjami innych)
    // stop - zadnego z czynnikow nie da sie przesunac
    // ---------// ---------// ---------// ---------// ---------
    // 0001 = 1 :     1       1
    // ---------
    // 0010 = 2 :     2       2, 1+1
    // ---------
    // 0011 = 3 :     1       2+1
    // ---------
    // 0100 = 4 :     3       4, 2+2, 2+1+1
    // ---------
    // 0101 = 5 :     2       4+1, 2+2+1
    // ---------
    // 0110 = 6 :     3       4+2, 4+1+1, 2+2+1+1
    // ---------
    // 0111 = 7 :     1       4+2+1
    // ---------
    // 1000 = 8 :     4       8, 4+4, 4+2+2, 4+2+1+1
    // ---------
    // 1001 = 9 :     3       8+1, 4+4+1, 4+2+2+1
    // ---------
    // 1010 = 10 :    5       8+2,  8+1+1,  4+4+2   4+4+1+1  4+2+2+1+1
    // ---------
    // 1011 = 11 :    2       8+2+1, 4+4+2+1
    // ---------
    // 10000 = 16 :   5       16,  8+8 , 8+4+4,  8+4+2+2,  8+4+2+1+1
    // ---------
    // 10011 = 19 :   3       16+2+1 , 8+8+2+1, 8+4+4+2+1,
    // ---------
    // 10100 = 20 :   8       16+4  , 16+2+2, 16+2+1+1     8+8+4, 8+8+2+2, 8+8+2+1+1,     8+4+4+2+2, 8+4+4+2+1+1
    // ---------
    // 10101 = 21 :   5       16+4+1, 16+2+2+1,   8+8+4+1, 8+8+2+2+1,   8+4+4+2+2+1
    // ---------
    // 101001= 41 :   8       32+8+1, 32+4+4+1,  32+4+2+2+1,        16+16+8+1, 16+16+4+4+1,  16+16+4+2+2+1       16+8+8+4+4+1, 16+8+8+4+2+2+1,
    // ---------
    // 101011= 43 :   5       32+8+2+1, 32+4+4+2+1,   16+16+8+2+1, 16+16+4+4+2+1,  16+8+8+4+4+2+1
    // ---------
    // 1010101 =
    // ---------
    // jesli tylko 1 1-ka - to rozwiazan jest pozycja 1-ki + 1 (czyli ilosc zer  + 1)
    // (ilosci zer +1) pomnozone przez siebie i 1    +   2 * ( ilosc tych grup zer - 1)
    // 1 + ( ilosci zer + najwgroupa? 0:1 ) pomnozone przez siebie i 1


  def numberOfWays(X: Int): Int = {
    var count = 0;

    var num = X;

    var total = 1;

    while(num > 0){
      println(" -- Iter:" + num)
      count = count + 1;
      if(num%2 != 0){

        total = (count + 1 ) * total - 1
        count = 0;
      }
      num = num>>1;
    }
    total;
  }

  def numberOfWays(X: String): BigInt = {
    var count = BigInt.apply(0);;
    var num = BigInt.apply(X);
    var total = BigInt.apply(1);;
    while(num > 0){
      count = count + 1;
      if(num%2 != 0){
        total = (count + 1 ) * total - 1
        count = 0;
      }
      num = num>>1;
    }
    total;
  }

  def main(args: Array[String]) {
    println("10000000000000000000000000000dec:" + numberOfWays("10000000000000000000000000000"));
    (1 to 100).foreach( item => println(item + " : " + numberOfWays(String.valueOf(item))))
//    println("1010:" + numberOfWays("10"));
//    println("1011:" + numberOfWays("11"));
//    println("1000:" + numberOfWays("8"));
//    println("10000:" + numberOfWays("16"));
//    println("10011:" + numberOfWays("19"));
    //print(numberOfWays(readInt()));
  }
}
