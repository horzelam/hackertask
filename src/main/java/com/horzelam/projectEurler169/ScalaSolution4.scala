package com.horzelam.projectEurler169

import sun.misc.FloatingDecimal.BinaryToASCIIConverter

/**
  * Created by osboxes on 07/01/17.
  */
object ScalaSolution4 {





    // 1010 = 10 = 8 + 2
    // ---------
    // ---------
    // na ile sposobow mozna poprzesuwac w prawo 1-ki
    // ---------
    // 1010 = 1000 + (0010)= 8 + 2 (rozlozenie na czynniki 1-sze) - (ostatni czynnik v1 - oruginalna kombinacja)
    // 0100 + 0100 + (0010) = 4+4+2 (ostatni czynnik v1 z wsztstkimi sensownymi kombinacjami innych)
    // ...
    // 1000 + (00001+ 0001) = 4+4+1+1 (ostatni czynnik v2 przesuniety o 1  z wsztstkimi sensownymi kombinacjami innych)
    // 0100 + 0100 + (0001 + 0001) = 4+4+1+1 (ostatni czynnik v2  przesuniety o 1  z wsztstkimi sensownymi kombinacjami innych)
    // 0100 + 0010 + 0010 + (0001 + 0001) = 4+2+2+1+1 (ostatni czynnik v2  przesuniety o 1  z wsztstkimi sensownymi kombinacjami innych)
    // stop - zadnego z czynnikow nie da sie przesunac
    // ---------// ---------// ---------// ---------// ---------
    // 0001 = 1 :     1       1
    // ---------
    // 0010 = 2 :     2       2, 1+1
    // ---------
    // 0011 = 3 :     1       2+1
    // ---------
    // 0100 = 4 :     3       4, 2+2, 2+1+1
    // ---------
    // 0101 = 5 :     2       4+1, 2+2+1
    // ---------
    // 0110 = 6 :     3       4+2, 4+1+1, 2+2+1+1
    // ---------
    // 0111 = 7 :     1       4+2+1
    // ---------
    // 1000 = 8 :     4       8, 4+4, 4+2+2, 4+2+1+1
    // ---------
    // 1001 = 9 :     3       8+1, 4+4+1, 4+2+2+1
    // ---------
    // 1010 = 10 :    5       8+2,  8+1+1,  4+4+2   4+4+1+1  4+2+2+1+1
    // ---------
    // 1011 = 11 :    2       8+2+1, 4+4+2+1
    // ---------
    // 1100 = 12 :    5       8+4, 8+2+2  8+2+1+1  (z cofnietym:) 4+4+2+2 4+4+2+1+1
    // ---------
    // 1110 = 14 :    4       8+4+2, 8+4+1+1  (z cofnietym:) 8+2+2+1+1 (next z cofnietym) 4+4+2+2+1+1
    // ---------
    // 10000 = 16 :   5       16,  8+8 , 8+4+4,  8+4+2+2,  8+4+2+1+1
    // ---------
    // 10011 = 19 :   3       16+2+1 , 8+8+2+1, 8+4+4+2+1,
    // ---------
    // 10100 = 20 :   8       16+4  , 16+2+2, 16+2+1+1     8+8+4, 8+8+2+2, 8+8+2+1+1,     8+4+4+2+2, 8+4+4+2+1+1
    // ---------
    // 10101 = 21 :   5       16+4+1, 16+2+2+1,   8+8+4+1, 8+8+2+2+1,   8+4+4+2+2+1
    // ---------
    // 101001= 41 :   8       32+8+1, 32+4+4+1,  32+4+2+2+1,        16+16+8+1, 16+16+4+4+1,  16+16+4+2+2+1       16+8+8+4+4+1, 16+8+8+4+2+2+1,
    // ---------
    // 101011= 43 :   5       32+8+2+1, 32+4+4+2+1,   16+16+8+2+1, 16+16+4+4+2+1,  16+8+8+4+4+2+1
    // ---------
    // 1010101 =
    // ---------
    // jesli tylko 1 1-ka - to rozwiazan jest pozycja 1-ki + 1 (czyli ilosc zer  + 1)
    // (ilosci zer +1) pomnozone przez siebie i 1    +   2 * ( ilosc tych grup zer - 1)
    // 1 + ( ilosci zer + najwgroupa? 0:1 ) pomnozone przez siebie i 1

  // -- some util functions to print binaries
  def fillBinary (bi: BigInt, len: Int) = {
    val s = toBinDigits (bi)
    if (s.length >= len) s
    else (List.fill (len-s.length) ("0")).mkString ("") + s
  }
  def toBinDigits (bi: BigInt): String = {
    if (bi == 0) "" else toBinDigits (bi /2) + (bi % 2)
  }


  def numberOfWays(X: String): BigInt = {
    val toAnalyze = BigInt.apply(X);
    if(toAnalyze <= 0) return 1;
    numberOfWays(toAnalyze);
  }

  def modify(toModify: BigInt): BigInt = {
    // For:
    // 110 it should return 011
    // 101 it should return 011
    // 100 it should return 010
    // 111 it should return 0
    // 11001100 it should return 01101100
    // 1 it should return 0
    // 0 it should return 0
    // in general it shifts the most significant to right , returns 0 if shifting wasnt possible
    var analyzedBitNR = toModify.bitLength-1
    var result:BigInt = 0
    while(analyzedBitNR >= 0 && result==0){
      if(toModify.testBit(analyzedBitNR)){
        analyzedBitNR  = analyzedBitNR - 1
      }
      else{
        // shift analyzedBitNR most significant bits to left
        result = toModify.setBit(analyzedBitNR)
        result = result.clearBit(toModify.bitLength-1)
      }
    }
    return result;

  }

  val cache = collection.mutable.Map[BigInt, BigInt]()

  def numberOfWays(x: BigInt): BigInt = {
    if( x <= BigInt.apply(0))
      return BigInt.apply(0);
    var analyzed = x;
    //println(x + " dec")
    //println(toBinDigits(x) + " bin")
    //println("---finish---")
    var countZeros = 0;
    var total:BigInt = 1;
    var lastPart:BigInt = 0;
    var analyzedBitNr = 0;
    while( analyzedBitNr < x.bitLength) {
      if(!analyzed.testBit(analyzedBitNr)) {
        countZeros = countZeros + 1;
      }
      else {
        val modified = modify(lastPart);
        if(!cache.contains(modified)){
          cache(modified) = numberOfWays(modified)
          //println("--cache recalc for " + modified + " from " + lastPart + " cache result: " + cache(modified))
        } else{
          //println("--cache hit for " + modified + " from " + lastPart + " cache result: " + cache(modified))
        }

        total = total * (countZeros + 1) + cache(modified)
        countZeros = 0
        lastPart = lastPart.setBit(analyzedBitNr)
      }
      analyzedBitNr = analyzedBitNr+1
    }
    total;
  }


  def main(args: Array[String]) {

          testModify()
          testNumberOfWays()

    //(1 to 100).foreach( item => println(item + " : " + numberOfWays(String.valueOf(item))))
    //println(numberOfWays("5"))
    //println(numberOfWays("43"))   // 101011= 43 -> 5 ways
    // println(numberOfWays("10000000000000000000000000000"))  //20679812321584
    // result 20679812321584 is it right ?


  }


  def testModify(): Unit ={
    // to test modification
    assert(modify(4) == 2)// 100 it should return 010 2dec
    assert(modify(5) == 3)// 101 it should return 011 3dec
    assert(modify(6) == 3)// 110 it should return 011 3dec
    assert(modify(7) == 0)// 111 it should return 0 0dec
    assert(modify(204) == 108) // 11001100 it should return 01101100   108dec
    assert(modify(1) == 0)
    assert(modify(0) == 0)
  }

  def testNumberOfWays(): Unit ={
    // TODO : why we have to return 1 for 0 ?!: and how it affects the rest
    assert(numberOfWays("0") ==     1 )

    assert(numberOfWays("1") ==     1 )
    assert(numberOfWays("2") ==     2 )
    assert(numberOfWays("3") ==     1 )
    assert(numberOfWays("4") ==     3 )
    assert(numberOfWays("5") ==     2 )
    assert(numberOfWays("6") ==     3 )
    assert(numberOfWays("7") ==     1 )
    assert(numberOfWays("8") ==     4 )
    assert(numberOfWays("9") ==     3 )
    assert(numberOfWays("10") ==    5 )
    assert(numberOfWays("11") ==    2 )
    assert(numberOfWays("12") ==    5 )
    assert(numberOfWays("14") ==    4 )
    assert(numberOfWays("16") ==   5 )
    assert(numberOfWays("19") ==   3 )
    assert(numberOfWays("20") ==   8 )
    assert(numberOfWays("21") ==   5 )
    assert(numberOfWays("41") ==   8 )
    assert(numberOfWays("43") ==   5 )

  }

}
