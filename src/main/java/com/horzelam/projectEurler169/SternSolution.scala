package com.horzelam.projectEurler169

/**
  * Created by osboxes on 11/01/17.
  */
object SternSolution {



  def numberOfWays(X: String):BigInt = {
    val toAnalyze = BigInt.apply(X);
    if(toAnalyze <= 0) return 1;
    numberOfWays(toAnalyze);
  }


  def numberOfWays(toAnalyze: BigInt):BigInt = {



    //hardcoded
    if(toAnalyze == 1) return 1;
    if(toAnalyze == 2) return 2;
    if(toAnalyze == 3) return 1;
    if(toAnalyze == 4) return 3;
    if(toAnalyze == 5) return 2;
    if(toAnalyze == 6) return 3;
    if(toAnalyze == 7) return 1;
    if(toAnalyze == 8) return 4;
    if(toAnalyze == 9) return 3;
    if(toAnalyze == 10) return 5;
    if(toAnalyze == 11) return 2;
    if(toAnalyze == 12) return 5;


    // is even
    if(toAnalyze % 2 == 0){
      return numberOfWays(toAnalyze/2)
    } else{
      return numberOfWays((toAnalyze-1)/2) + numberOfWays((toAnalyze+1)/2)
    }

  }

  def main(args: Array[String]) {


    println("Not working as expected")

    //(1 to 100).foreach( item => println(item + " : " + numberOfWays(String.valueOf(item))))
    println(numberOfWays("5"))
    println(numberOfWays("43"))   // 101011= 43 -> 5 ways
    //println(numberOfWays("10000000000000000000000000000"))
    // result 20679812321584 is it right ?


  }


}
