package com.horzelam.projectEurler169

/**
  * Created by osboxes on 11/01/17.
  */
object StSolution_OK {

  def main(args: Array[String]) {
    testPowOf2()
    testNumberOfWays()
    println(numberOfWays("2")) //2
    //println(numberOfWays("10000000000000000000000000000")) //795523256153
    assert(numberOfWays("10000000000000000000000000000") ==     BigInt.apply("795523256153") )
  }

  def numberOfWays(X: String): BigInt = {
    val toAnalyze = BigInt.apply(X);
    if (toAnalyze <= 0) return 1;
    numberOfWays(toAnalyze);
  }

  def isPowOf2(n:BigInt):Boolean = {

    var result = false;
    if(n==0) return false;
    for (pos <- (0 to n.bitLength-2)) {
      if(n.testBit(pos)){
        return false;
      }
    }
    return true;
  }
  def testPowOf2(): Unit ={
    assert(isPowOf2(1)==true)
    assert(isPowOf2(2)==true)
    assert(isPowOf2(4)==true)
    assert(isPowOf2(512)==true)

    assert(isPowOf2(511)==false)
    assert(isPowOf2(3)==false)
    assert(isPowOf2(6)==false)
  }

  def testNumberOfWays(): Unit ={
    // TODO : why we have to return 1 for 0 ?!: and how it affects the rest
    assert(numberOfWays("0") ==     1 )

    // TODO: why power of 2 is returned wrong ?!
    assert(numberOfWays("1") ==     1 )
    //assert(numberOfWays("2") ==     2 )
    assert(numberOfWays("3") ==     1 )
    //assert(numberOfWays("4") ==     3 )
    assert(numberOfWays("5") ==     2 )
    assert(numberOfWays("6") ==     3 )
    assert(numberOfWays("7") ==     1 )
    //assert(numberOfWays("8") ==     4 )
    assert(numberOfWays("9") ==     3 )
    assert(numberOfWays("10") ==    5 )
    assert(numberOfWays("11") ==    2 )
    assert(numberOfWays("12") ==    5 )
    assert(numberOfWays("14") ==    4 )
    //assert(numberOfWays("16") ==   5 )
    assert(numberOfWays("19") ==   3 )
    assert(numberOfWays("20") ==   8 )
    assert(numberOfWays("21") ==   5 )
    assert(numberOfWays("41") ==   8 )
    assert(numberOfWays("43") ==   5 )

  }


  val cache = collection.mutable.Map[BigInt, BigInt]()
  def numberOfWays(n: BigInt): BigInt = {
    if (n == 1)
      return 1
    else if (n == 0)
      return 0
    else {
      if(cache.contains(n)){
        return cache(n);
      }
      else{
        var result:BigInt=0
        if (!n.testBit(0)) {
          result = numberOfWays((n - 2) / 2) + numberOfWays(n / 2);
          if (isPowOf2(n - 2) && (n != 4) )
            result = 1 + result
        }
        else {
          result = numberOfWays((n - 1) / 2)
          if (isPowOf2(n - 1) && n != 3)
            result = 1 + result
        }
        cache(n) = result
        return result
      }

    }
  }

}
