package com.horzelam.projectEurler169

/**
  * Created by osboxes on 07/01/17.
  */
object ScalaSolution {

  def findMax(min:Int, X:Int): Int = {
    var i = min;

    while( X - (2<<i) > 0){
      i = i+1;
    }
    return i;
  }

  def numberOfWays(min: Int, X: Int, prev:Int): Int = {

    // println("Iter - Min: " + Min + " , X: " + X + " -- "+ list)
    var maxChecked = findMax(min, X)
    //math.ceil(math.sqrt(X)).toInt; //its too much , but it's just estimation

    // check if min was already used:
    //var start = if(min == prev) min + 1 else min;

    (min to maxChecked).map(candidate => {
      //println(((1 to Min).map(a=>"-").foldLeft("")(_+_)) + " candidate : " + candidate)
      var left = (X - (2<<candidate)).toInt;

      if (left == 0) {
        //println(((1 to Min).map(a=>"-").foldLeft("")(_+_))  + " FOUND solution: " + (list ++ List(candidate)))
        1;
      }
      else if (left > 0) {
        var next = if( candidate == prev ) candidate + 1 else candidate;
        numberOfWays( next, left , candidate )
      }
      else {
        0;
      }
    }).foldLeft(0)(_ + _);

  }

  def numberOfWays(X: Int): Int = {
    numberOfWays(0, X, -1);
  }

  def main(args: Array[String]) {
    //println(numberOfWays(readInt(),readInt()))
    //println("10,2  : " + numberOfWays(10, 2));

    //println("somehitn:" + ( 2<<3))
    print(numberOfWays(0,readInt(),-1));
    //println("100,3 : " + numberOfWays(100, 3));
  }
}
