package com.horzelam.LongestPalindromicSubstring;

import java.util.Stack;

import static org.assertj.core.api.Assertions.assertThat;

public class LongestPalindromicSubstring {
    public static void main(String[] args) {
        final LongestPalindromicSubstring main = new LongestPalindromicSubstring();
        String s;

                s = "babad";
                assertThat(main.longestPalindrome(s)).isIn("aba", "bab");

                s = "aa";
                assertThat(main.longestPalindrome(s)).isEqualTo("aa");

                s = "va";
                assertThat(main.longestPalindrome(s)).isEqualTo("v");
        s = "ccc";
        assertThat(main.longestPalindrome(s)).isEqualTo("ccc");

        s = "babababbaba";
        assertThat(main.longestPalindrome(s)).isEqualTo("ababbaba");
    }

    public String longestPalindrome(String s) {

        if (s.length() == 1) {
            return s;
        }
        if (s.length() < 3) {
            if (s.charAt(0) == s.charAt(1)) {
                return s;
            } else {
                return s.substring(0, 1);
            }
        }

        int maxFound = 1;
        int maxStart = 0;
        int maxEnd = 0;
        for (int i = 0; i < (s.length() - (maxEnd-maxStart)/2); i++) {

            if (i > 0) {
                if (i > 1 && s.charAt(i - 2) == s.charAt(i)) {
                    //System.out.println("Middle candidate at " + (i-1) + " - " + s.charAt(i - 1));
                    int start = i - 2;
                    int end = i;
                    while ((start - 1) >= 0 && (end + 1) < s.length() && s.charAt(start - 1) == s.charAt(end + 1)) {
                        start--;
                        end++;
                    }
                    if (end - start + 1 > maxFound) {
                        maxFound = end - start + 1;
                        maxStart = start;
                        maxEnd = end;

                        //                        System.out.println("--maxStart:" + maxStart);
                        //                        System.out.println("--maxEnd:" + maxEnd);
                        //                        System.out.println("--word:" +  s.substring(maxStart, maxEnd + 1));
                    }
                }
                if (s.charAt(i - 1) == s.charAt(i)) {
                    //System.out.println("Right candidate at " + i + " - " + s.charAt(i));
                    int start = i - 1;
                    int end = i;
                    while ((start - 1) >= 0 && (end + 1) < s.length() && s.charAt(start - 1) == s.charAt(end + 1)) {
                        start--;
                        end++;
                    }
                    //System.out.println("From right found: " + start + " -- " + end );
                    if ((end - start + 1) > maxFound) {
                        maxFound = end - start + 1;
                        maxStart = start;
                        maxEnd = end;

                        //                        System.out.println("--maxStart:" + maxStart);
                        //                        System.out.println("--maxEnd:" + maxEnd);
                        //                        System.out.println("--word:" +  s.substring(maxStart, maxEnd + 1));
                    }
                }

            }

        }

        //        System.out.println("--------maxStart:" + maxStart);
        //        System.out.println("--------maxEnd:" + maxEnd);
        //        System.out.println("--------word:" +  s.substring(maxStart, maxEnd + 1));
        return s.substring(maxStart, maxEnd + 1);
    }

}