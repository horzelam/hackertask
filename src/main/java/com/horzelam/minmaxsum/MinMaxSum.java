package com.horzelam.minmaxsum;


import java.util.Scanner;

public class MinMaxSum {

    // see https://www.hackerrank.com/challenges/mini-max-sum
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int[] arr = new int[5];
        int minIndex = -1;
        int maxIndex = -1;
        int min = Integer.MAX_VALUE;
        int max = -1;
        for (int arr_i = 0; arr_i < 5; arr_i++) {
            arr[arr_i] = in.nextInt();
            if (arr[arr_i] < min) {
                min = arr[arr_i];
                minIndex = arr_i;
            }
            if (arr[arr_i] > max) {
                max = arr[arr_i];
                maxIndex = arr_i;
            }
        }
        long maxSum = 0;
        long minSum = 0;
        for (int arr_i = 0; arr_i < 5; arr_i++) {
            if (arr_i != minIndex) {
                maxSum += arr[arr_i];
            }
            if (arr_i != maxIndex) {
                minSum += arr[arr_i];
            }
        }
        System.out.println(minSum + " " + maxSum);


    }
}