package com.horzelam.codility_vervica;

import java.util.HashSet;
import java.util.Set;

public class TreeDistinctEdgesFind {
    // you can also use imports, for example:
    // import java.util.*;
    static class Tree {
        public int x;
        public Tree l;
        public Tree r;

        public Tree(final int x, Tree l, Tree r) {
            this.x = x;
            this.l = l;
            this.r = r;
        }
    }

    // you can write to stdout for debugging purposes, e.g.
    // System.out.println("this is a debug message");
    public static void main(String[] args) {
        Tree root1 = new Tree(4,
                        new Tree(5,
                            new Tree(4,
                                    new Tree(5,
                                        null,
                                          null
                                    )
                                    , null
                            )
                            ,null
                        ),
                        new Tree(6,
                            new Tree (1, null,null),
                            new Tree (6, null,null)
                        )

                );
        System.out.println("result 1:" +  solution(root1));

        Tree root2 = new Tree(3,
                    null,
                       new Tree(5,
                            null,
                               new Tree(8,
                                    new Tree(20,
                                            new Tree(21,null,null),
                                            null
                                    ),
                                    new Tree(3,
                                            null,
                                            new Tree(5,
                                                    null,
                                                    new Tree(7, null,null))

                                    )
                               )
                       )
        );
        System.out.println("result 2:" +  solution(root2));
    }

    public static int solution(Tree T) {
        // write your code in Java SE 8
        final Set<Integer> foundDistinctValues = new HashSet<>();

        return subSolution(T, foundDistinctValues);
    }

    public static int subSolution(Tree T, Set<Integer> foundDistinctValues) {

        if (T == null) {
            return foundDistinctValues.size();
        }
        final HashSet<Integer> currentDistinctValues = new HashSet<>(foundDistinctValues);
        currentDistinctValues.add(T.x);

        int lsolution = subSolution(T.l, currentDistinctValues);
        int rsolution = subSolution(T.r, currentDistinctValues);

        return lsolution > rsolution ? lsolution: rsolution;
    }
    /** SUBMITED


     // you can also use imports, for example:
     // import java.util.*;

     // you can write to stdout for debugging purposes, e.g.
     // System.out.println("this is a debug message");
     import java.util.HashSet;
     import java.util.Set;

     class Solution {
     public int solution(Tree T) {
     // write your code in Java SE 8
     final Set<Integer> foundDistinctValues = new HashSet<>();

     return subSolution(T, foundDistinctValues);
     }


     // Naive approach with copying the Set of distinct values across on every node
     public int subSolution(Tree T, Set<Integer> foundDistinctValues) {

     if (T == null) {
     return foundDistinctValues.size();
     }
     final HashSet<Integer> currentDistinctValues = new HashSet<>(foundDistinctValues);
     currentDistinctValues.add(T.x);

     int lsolution = subSolution(T.l, currentDistinctValues);
     int rsolution = subSolution(T.r, currentDistinctValues);

     return lsolution > rsolution ? lsolution: rsolution;
     }

     }


     */

}
