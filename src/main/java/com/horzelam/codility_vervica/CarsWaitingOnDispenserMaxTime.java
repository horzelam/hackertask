package com.horzelam.codility_vervica;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

public class CarsWaitingOnDispenserMaxTime {

    public static void main(String[] args) {

        int[] nums1 = {2, 8, 4, 3, 2};
        int x1 = 7, y1 = 11, z1 = 3;
        System.out.println("Excepted: 8 , current: " + subSolution(nums1, x1, y1, z1));

        int[] nums2 = {5};
        int x2 = 4, y2 = 0, z2 = 3;
        System.out.println("Excepted: -1 , current: " + subSolution(nums2, x2, y2, z2));
    }

    static class Dispenser {
        int freeFuel;
        int usedFuel;

        public Dispenser(final int freeFuel, final int usedFuel) {
            this.freeFuel = freeFuel;
            this.usedFuel = usedFuel;
        }
    }

    private static int subSolution(int[] cars, int xFuel, int yFuel, int zFuel) {

        // dispensers map by key which is numbe of dispenser : X-key:0 , Y-key:1, Z-key:2
        // value array: amountOfLeftFuel, amountOfUsedFuel
        final Map<Integer, int[]> dispenserMap = new HashMap<>();
        dispenserMap.put(0, new int[]{xFuel, 0});
        dispenserMap.put(1, new int[]{yFuel, 0});
        dispenserMap.put(2, new int[]{zFuel, 0});

        // to order Dispensers by free liters, then by used fuel and finally by order X,Y,Z
        final Queue<Map.Entry<Integer, int[]>> currentFreeDispensers = new PriorityQueue<>(
                (a, b) -> a.getValue()[0] == b.getValue()[0] ? a.getValue()[1] - b.getValue()[1] : a.getKey() - b.getKey());
        currentFreeDispensers.addAll(dispenserMap.entrySet());

        final int[] waitingTimes = new int[3];

        final List<Map.Entry<Integer, int[]>> currentReviewedDispensers = new ArrayList<>();

        for (int carIndex = 0; carIndex < cars.length; carIndex++) {

            final int carFuelDemand = cars[carIndex];

            while (!currentFreeDispensers.isEmpty()) {
                // if there are free dispensers - take first

                final Map.Entry<Integer, int[]> dispenser = currentFreeDispensers.poll();

                if (dispenser.getValue()[0] < carFuelDemand) {
                    //if  car demand is bigger than current free dispenser supply
                    currentReviewedDispensers.add(dispenser);
                    if (currentFreeDispensers.isEmpty()) {
                        return -1;
                    }
                } else {
                    // we can fuel this car
                    dispenser.getValue()[0] -= carFuelDemand;
                    dispenser.getValue()[1] += carFuelDemand;
                    currentReviewedDispensers.add(dispenser);
                    // if it's last car
                    if (carIndex + 1 == cars.length) {
                        break;
                    }
                    waitingTimes[dispenser.getKey()] += carFuelDemand;
                    // release dispensers
                    currentFreeDispensers.addAll(currentReviewedDispensers);
                    currentReviewedDispensers.clear();
                    // and break a review dispensers loop - go to next car
                    break;
                }
            }
        }
        return Math.max(waitingTimes[0], Math.max(waitingTimes[1], waitingTimes[2]));
    }

}
