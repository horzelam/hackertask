package com.horzelam.codility_vervica;

import java.math.BigInteger;

public class BinaryBit {
    // you can also use imports, for example:
    // import java.util.*;

    // you can write to stdout for debugging purposes, e.g.
    // System.out.println("this is a debug message");
    public static void main(String[] args) {
        System.out.println("bin:" + Integer.toBinaryString(100000000 * 100000000) + " , res:" + solution(100000000, 100000000));
        System.out.println("bin:" + Integer.toBinaryString(3 * 7) + " , res:" + solution(3, 7));
        System.out.println("bin:" + Integer.toBinaryString(0 * 0) + " , res:" + solution(0, 0));
        System.out.println("bin:" + Integer.toBinaryString(1 * 1) + " , res:" + solution(1, 1));
        System.out.println("bin:" + Integer.toBinaryString(2 * 2) + " , res:" + solution(2, 2));
    }

    public static int solution(int A, int B) {
        // write your code in Java SE 8
        BigInteger mul = BigInteger.valueOf(A).multiply(BigInteger.valueOf(B));
        System.out.println(" mul: " + mul + " bin: " + mul.toString(2));
        BigInteger count = BigInteger.ZERO;
        while (mul.compareTo(BigInteger.ZERO) > 0) {
            count = count.add(mul.mod(BigInteger.valueOf(2)));
            mul = mul.divide(BigInteger.valueOf(2));
        }
        return count.intValue();
    }

}
