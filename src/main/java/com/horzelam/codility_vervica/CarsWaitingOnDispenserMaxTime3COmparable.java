package com.horzelam.codility_vervica;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

public class CarsWaitingOnDispenserMaxTime3COmparable {

    public static void main(String[] args) {
        int[] nums1 = {2, 8, 4, 3, 2};
        int x1 = 7, y1 = 11, z1 = 3;
        System.out.println("Excepted: 8 , current: " + subSolution(nums1, x1, y1, z1));

        int[] nums2 = {5};
        int x2 = 4, y2 = 0, z2 = 3;
        System.out.println("Excepted: -1 , current: " + subSolution(nums2, x2, y2, z2));
    }

    static class Dispenser implements Comparable<Dispenser> {
        int orderKey;
        int freeFuel;
        int usedFuel;

        public Dispenser(final int orderKey, final int freeFuel, final int usedFuel) {
            this.orderKey = orderKey;
            this.freeFuel = freeFuel;
            this.usedFuel = usedFuel;
        }

        public int getOrderKey() {
            return orderKey;
        }

        public boolean isEnoughFuel(final int carFuelDemand) {
            return freeFuel >= carFuelDemand;
        }

        public void consumeFuel(final int fuelAmount) {
            this.freeFuel -= fuelAmount;
            this.usedFuel += fuelAmount;
        }

        @Override
        public int compareTo(final Dispenser other) {
            return this.freeFuel == other.freeFuel ? this.usedFuel - other.usedFuel : this.orderKey - other.orderKey;
        }

    }

    private static int subSolution(int[] cars, int xFuel, int yFuel, int zFuel) {

        // To order available Dispensers :
        // - by free liters,
        // - then by used fuel
        // - then by order : X,Y,Z
        final Queue<Dispenser> availableSortedDispensers = new PriorityQueue<>();
        availableSortedDispensers.add(new Dispenser(0, xFuel, 0));
        availableSortedDispensers.add(new Dispenser(1, yFuel, 0));
        availableSortedDispensers.add(new Dispenser(2, zFuel, 0));

        final int[] waitingTimes = new int[3];

        final List<Dispenser> currentReviewedDispensers = new ArrayList<>();

        for (int carIndex = 0; carIndex < cars.length; carIndex++) {

            final int carFuelDemand = cars[carIndex];

            while (!availableSortedDispensers.isEmpty()) {
                // if there are free dispensers - take first

                final Dispenser entry = availableSortedDispensers.poll();

                if (!entry.isEnoughFuel(carFuelDemand)) {
                    //if  car demand is bigger than current free dispenser supply
                    currentReviewedDispensers.add(entry);
                    if (availableSortedDispensers.isEmpty()) {
                        return -1;
                    }
                } else {
                    // we can fuel this car
                    entry.consumeFuel(carFuelDemand);
                    currentReviewedDispensers.add(entry);
                    // if it's last car
                    if (carIndex + 1 == cars.length) {
                        break;
                    }
                    waitingTimes[entry.getOrderKey()] += carFuelDemand;
                    // release dispensers
                    availableSortedDispensers.addAll(currentReviewedDispensers);
                    currentReviewedDispensers.clear();
                    // and break a review dispensers loop - go to next car
                    break;
                }
            }
        }
        return Math.max(waitingTimes[0], Math.max(waitingTimes[1], waitingTimes[2]));
    }

}
