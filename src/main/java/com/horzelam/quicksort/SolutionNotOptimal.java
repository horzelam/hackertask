package com.horzelam.quicksort;

import java.util.Scanner;

/**
 * Created by morele on 2017-02-26.
 */
public class SolutionNotOptimal {


    static void partition_part_1(int[] ar) {
        int pivot = ar[0];
        int[] right = new int[ar.length];
        int[] left = new int[ar.length];
        int leftSize = 0;
        int rightSize = 0;
        for (int i = 1; i < ar.length; i++) {
            if (ar[i] < pivot) {
                left[leftSize++] = ar[i];
            } else {
                right[rightSize++] = ar[i];
            }
        }
        int i = 0;
        for (; i < leftSize; i++) {
            ar[i] = left[i];
        }
        ar[i++] = pivot;
        for (int r = 0; r < rightSize; r++) {
            ar[i++] = right[r];
        }
    }


    static void quickSort(int[] ar) {
        // partition
        int pivot = ar[0];
        int[] left = new int[ar.length];
        int[] right = new int[ar.length];
        int leftSize = 0;
        int rightSize = 0;
        for (int i = 1; i < ar.length; i++) {
            if (ar[i] < pivot) {
                left[leftSize++] = ar[i];
            } else {
                right[rightSize++] = ar[i];
            }
        }

        int[] leftToSort = new int[leftSize];
        if(leftSize>1){
            System.arraycopy(left, 0, leftToSort, 0, leftSize);
            quickSort(  leftToSort );
        }else if(leftSize==1){
            leftToSort[0] = left[0];
        }

        int[] rightToSort = new int[rightSize];
        if(rightSize>1){
            System.arraycopy(right, 0, rightToSort, 0, rightSize);
            quickSort(  rightToSort );
        }else if(rightSize==1){
            rightToSort[0] = right[0];
        }

        // merge
        int i = 0;
        for (; i < leftSize; i++) {
            ar[i] = leftToSort[i];
        }
        ar[i++] = pivot;
        for (int r = 0; r < rightSize; r++) {
            ar[i++] = rightToSort[r];
        }

        printArray(ar);
    }

    static void printArray(int[] ar) {
        for (int n : ar) {
            System.out.print(n + " ");
        }
        System.out.println("");
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] ar = new int[n];
        for (int i = 0; i < n; i++) {
            ar[i] = in.nextInt();
        }
        // --- PART 1 ----
        //partition_part_1(ar)
        //printArray(ar);


        // complete approach:
        quickSort(ar);
    }

    /**
     Input:
     5
     4 5 3 7 2
     -- Unique elements in range -1000..1000 , 1 <= amount <= 1000
     -- Multiple answer can exists for the given test case. Print any one of them.
     Expected Output:
     3 2 4 5 7
     */
}
