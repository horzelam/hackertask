package com.horzelam.quicksort;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * This version sorts in place
 */
public class Solution {


    static int partition(int[] ar, int start, int end) {
        int pivot = ar[end];
        int pivotPos = start;

        // process all comparing to pivot
        for (int i = start; i < end ; i++) {
            if (ar[i] < pivot) {
                swap( ar, pivotPos, i );
                pivotPos++;
            }
        }

        swap(ar, pivotPos, end);
        printArray(ar);
        return pivotPos;
    }

    private static void swap(int[] ar, int oneBigger, int otherSmaller) {
        if(oneBigger == otherSmaller) return;
        int temp = ar[oneBigger];
        ar[oneBigger] = ar[otherSmaller];
        ar[otherSmaller] = temp;
    }


    static void quickSort(int[] ar, int start, int end) {
        // partition
        if(start == end){
            return;
        }

        int pivotPosition = partition(ar, start, end);
        if(start < pivotPosition - 1)
            quickSort(ar, start, pivotPosition - 1);
        if(end > pivotPosition + 1)
            quickSort(ar, pivotPosition + 1 , end);
    }



    static void printArray(int[] ar) {
        for (int n : ar) {
            System.out.print(n + " ");
        }
        System.out.println("");
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] ar = new int[n];
        for (int i = 0; i < n; i++) {
            ar[i] = in.nextInt();
        }
        quickSort(ar, 0, n-1);
    }

    /**
     Input:
     5
     4 5 3 7 2
     -- Unique elements in range -1000..1000 , 1 <= amount <= 1000
     -- Multiple answer can exists for the given test case. Print any one of them.
     Expected Output:
     3 2 4 5 7
     */
}
//