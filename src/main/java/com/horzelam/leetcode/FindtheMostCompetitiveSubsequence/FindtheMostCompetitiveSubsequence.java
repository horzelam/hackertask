package com.horzelam.leetcode.FindtheMostCompetitiveSubsequence;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class FindtheMostCompetitiveSubsequence {
    public static void main(String[] args) throws IOException {
        final FindtheMostCompetitiveSubsequence main = new FindtheMostCompetitiveSubsequence();
        int[] nums;
        int k;

        //        Input: nums = [3,5,2,6], k = 2
        //        Output: [2,6]
        //        Explanation: Among the set of every possible subsequence: {[3,5], [3,2], [3,6], [5,2], [5,6], [2,6]}, [2,6] is the most competitive.
        //        Example 2:
        nums = new int[]{3, 5, 2, 6};
        k = 2;
        assertThat(main.mostCompetitive(nums, k)).isEqualTo(new int[]{2, 6});
        //
        //        Input: nums = [2,4,3,3,5,4,9,6], k = 4
        //        Output: [2,3,3,4]
        nums = new int[]{2, 4, 3, 3, 5, 4, 9, 6};
        k = 4;
        assertThat(main.mostCompetitive(nums, k)).isEqualTo(new int[]{2, 3, 3, 4});

        long start = System.currentTimeMillis();
        List<String> lines = Files.readAllLines(Paths.get(
                "/home/michal/workspace-priv/task/src/main/java/com/horzelam/leetcode/FindtheMostCompetitiveSubsequence/bigcase.txt"));
        nums = read(lines.get(0));
        k = 50000;
        int[] res = main.mostCompetitive(nums, k);
        System.out.println("time: " + (System.currentTimeMillis() - start) + " res size: " + res.length);
        //assertThat(res).isEqualTo(new int[]{2, 3, 3, 4});



        start = System.currentTimeMillis();
        lines = Files.readAllLines(Paths.get(
                "/home/michal/workspace-priv/task/src/main/java/com/horzelam/leetcode/FindtheMostCompetitiveSubsequence/bigcase2.txt"));
        nums = read(lines.get(0));
        k = 78263;
        res = main.mostCompetitive(nums, k);
        System.out.println("time: " + (System.currentTimeMillis() - start) + " res size: " + res.length);


    }

    private static int[] read(final String s) {
        final String[] data = s.split(",");
        final int[] result = new int[data.length];
        int i = 0;
        for (final String datum : data) {
            result[i++] = Integer.parseInt(datum);
        }
        return result;
    }

    public int[] mostCompetitive(int[] nums, int k) {
        final int[] out = new int[k];

        int ind = 0;
        for (int i = 0; i < k; i++) {
            ind = findSmallest(nums, ind, nums.length - k + i);
            out[i] = nums[ind];
            ind = ind + 1;
        }
        return out;
    }

    private int findSmallest(final int[] nums, final int ind, final int end) {
        int min = 1000000001;
        int minInd = -1;
        int i;
        for (i = ind; i <= end; i++) {
            if (nums[i] < min) {
                min = nums[i];
                minInd = i;
            }
        }
        return minInd;
    }
}
