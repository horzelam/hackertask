package com.horzelam.leetcode.FindtheMostCompetitiveSubsequence;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class FindtheMostCompetitiveSubsequence_prevMin {
    public static void main(String[] args) throws IOException {
        final FindtheMostCompetitiveSubsequence_prevMin main = new FindtheMostCompetitiveSubsequence_prevMin();
        int[] nums;
        int k;

        //        //        Input: nums = [3,5,2,6], k = 2
        //        //        Output: [2,6]
        //        //        Explanation: Among the set of every possible subsequence: {[3,5], [3,2], [3,6], [5,2], [5,6], [2,6]}, [2,6] is the most competitive.
        //        //        Example 2:
        //        nums = new int[]{3, 5, 2, 6};
        //        k = 2;
        //        assertThat(main.mostCompetitive(nums, k)).isEqualTo(new int[]{2, 6});
        //
        //        Input: nums = [2,4,3,3,5,4,9,6], k = 4
        //        Output: [2,3,3,4]
        nums = new int[]{2, 4, 3, 3, 5, 4, 9, 6};
        k = 4;
        assertThat(main.mostCompetitive(nums, k)).isEqualTo(new int[]{2, 3, 3, 4});

        long start = System.currentTimeMillis();
        List<String> lines = Files.readAllLines(Paths.get(
                "/home/michal/workspace-priv/task/src/main/java/com/horzelam/leetcode/FindtheMostCompetitiveSubsequence/bigcase.txt"));
        nums = read(lines.get(0));
        k = 50000;
        int[] res = main.mostCompetitive(nums, k);
        System.out.println("time: " + (System.currentTimeMillis() - start) + " res size: " + res.length);
        //assertThat(res).isEqualTo(new int[]{2, 3, 3, 4});

        start = System.currentTimeMillis();
        lines = Files.readAllLines(Paths.get(
                "/home/michal/workspace-priv/task/src/main/java/com/horzelam/leetcode/FindtheMostCompetitiveSubsequence/bigcase2.txt"));
        nums = read(lines.get(0));
        k = 78263;
        res = main.mostCompetitive(nums, k);
        System.out.println("time: " + (System.currentTimeMillis() - start) + " res size: " + res.length);

    }

    private static int[] read(final String s) {
        final String[] data = s.split(",");
        final int[] result = new int[data.length];
        int i = 0;
        for (final String datum : data) {
            result[i++] = Integer.parseInt(datum);
        }
        return result;
    }

    public int[] mostCompetitive(int[] nums, int k) {
        final int[] out = new int[k];
        final int[] prevs = new int[nums.length];
        boolean prevInitialized = false;

        int ind = 0;
        for (int i = 0; i < k; i++) {
            if (prevInitialized) {
                prevs[nums.length - k + i] = nums.length - k + i;

                if (nums[prevs[ind]] <= nums[nums.length - k + i]) {
                    ind = prevs[ind];
                    recalc(nums, prevs, ind + 1, nums.length - k + i);
                } else {
                    ind = nums.length - k + i;
                }
            } else {
                ind = findSmallest(nums, prevs, ind, nums.length - k + i);
            }
            prevInitialized = true;
            out[i] = nums[ind];
            ind = ind + 1;
        }
        return out;
    }

    private void recalc(final int[] nums, final int[] prevs, final int ind, final int end) {
        int minInd = end;
        int min = nums[end];
        for (int i = end - 1 ; i >= ind; i--) {
            if (nums[i] <= min) {
                return;
            } else {
                prevs[i] = minInd;
            }
        }
    }

    private int findSmallest(final int[] nums, final int[] prevs, final int ind, final int end) {
        int min = 1000000001;
        int minInd = -1;
        int i;
        for (i = end; i >= ind; i--) {
            if (nums[i] <= min) {
                min = nums[i];
                minInd = i;
            }
            prevs[i] = minInd;
        }
        return minInd;
    }
}
