package com.horzelam.leetcode.AddTwoNumbers;

import java.util.Stack;

import static org.assertj.core.api.Assertions.assertThat;

public class AddTwoNumbers {
    public static void main(String[] args) {
        final AddTwoNumbers main = new AddTwoNumbers();
        ListNode l1;
        ListNode l2;
        ListNode res;

        // case
        // Input: l1 = [2,4,3], l2 = [5,6,4]
        // Output: [7,0,8]
        // Explanation: 342 + 465 = 807.
        //        l1 = new ListNode(2, new ListNode(4, new ListNode(3)));
        //        l2 = new ListNode(5, new ListNode(6, new ListNode(4)));
        //        res = main.addTwoNumbers(l1, l2);
        //        //System.out.println(res);
        //        assertThat(res).isEqualToComparingFieldByFieldRecursively(new ListNode(7, new ListNode(0, new ListNode(8))));
        //
        //        // case
        //        l1 = new ListNode(0);
        //        l2 = new ListNode(0);
        //        res = main.addTwoNumbers(l1, l2);
        //        //System.out.println(res);
        //        assertThat(res).isEqualToComparingFieldByFieldRecursively(new ListNode(0));

        //        l1 = new ListNode(2, new ListNode(4, new ListNode(3)));
        //        l2 = new ListNode(9);
        //        res = main.addTwoNumbers(l1, l2);
        //        System.out.println(res);
        //        assertThat(res).isEqualToComparingFieldByFieldRecursively( new ListNode(2, new ListNode(5, new ListNode(2))));

        l1 = new ListNode(9, new ListNode(9, new ListNode(9, new ListNode(9, new ListNode(9, new ListNode(9, new ListNode(9)))))));
        l2 = new ListNode(9, new ListNode(9, new ListNode(9, new ListNode(9))));
        res = main.addTwoNumbers(l1, l2);
        System.out.println(res);
        assertThat(res).isEqualToComparingFieldByFieldRecursively(new ListNode(8,
                new ListNode(9, new ListNode(9, new ListNode(9, new ListNode(0, new ListNode(0, new ListNode(0, new ListNode(1)))))))));
    }

    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode curr1 = l1;
        ListNode curr2 = l2;
        ListNode head = null;
        ListNode prev = null;
        int carry = 0;

        while (curr1 != null || curr2 != null) {

            int currVal = 0;
            if (curr1 != null) {
                currVal += curr1.val;
                curr1 = curr1.next;
            }
            if (curr2 != null) {
                currVal += curr2.val;
                curr2 = curr2.next;
            }
            int nodeVal = (currVal + carry) % 10;
            carry = (currVal + carry) / 10;
            if (head == null) {
                head = new ListNode(nodeVal);
                prev = head;
            } else {
                prev.next = new ListNode(nodeVal);
                prev = prev.next;
            }
        }
        if (carry > 0) {
            prev.next = new ListNode(carry);
        }
        return head;
    }

    public ListNode addTwoNumbers2(ListNode l1, ListNode l2) {
        ListNode curr1 = l1;
        ListNode curr2 = l2;
        Stack<Integer> stack1 = new Stack<>();
        Stack<Integer> stack2 = new Stack<>();
        while (curr1 != null || curr2 != null) {
            if (curr1 != null) {
                stack1.push(curr1.val);
                curr1 = curr1.next;
            }
            if (curr2 != null) {
                stack2.push(curr2.val);
                curr2 = curr2.next;
            }
        }

        ListNode prevNode = null;
        int carry = 0;
        ListNode result = null;
        while (!stack1.isEmpty() || !stack2.isEmpty()) {
            final int res = (stack1.isEmpty() ? 0 : stack1.pop()) + (stack2.isEmpty() ? 0 : stack2.pop());
            result = new ListNode(res % 10 + carry, prevNode);
            carry = res / 10;
            prevNode = result;
        }

        return result;
    }
    //
    //    private ListNode rec(final ListNode smaller, final ListNode bigger) {
    //        final ListNode nextSmaller = smaller.next;
    //        final ListNode nextBigger = bigger.next;
    //        if (nextSmaller != null && nextBigger != null) {
    //            rec(nextSmaller, next)
    //        }
    //    }

    static class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }

        @Override
        public String toString() {
            return "[" + val + ", " + next + " ]";
        }
    }
}
