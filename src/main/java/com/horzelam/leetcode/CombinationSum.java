package com.horzelam.leetcode;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Given an array of distinct integers candidates and a target integer target,
 * return a list of all unique combinations of candidates where the chosen numbers sum to target. You may return the
 * combinations in any order.
 * <p>
 * The same number may be chosen from candidates an unlimited number of times.
 * Two combinations are unique if the frequency of at least one of the chosen numbers is different.
 * <p>
 * It is guaranteed that the number of unique combinations that sum up to target is less than 150 combinations for
 * the given input.
 */
public class CombinationSum {


    private static void log(final String s) {
        System.out.println("....." + s);
    }

    public static void main(String[] args) {
        /// case 1
        int[] candidates = new int[]{2, 7, 6, 3, 5, 1};
        int target = 9;
        System.out.println("Current: " + combinationSum(candidates, target));
        System.out.println(" vs Expected: Output: [[1,1,1,1,1,1,1,1,1],[1,1,1,1,1,1,1,2],[1,1,1,1,1,1,3],[1,1,1,1,1," +
                "2,2],[1,1,1,1,2,3],[1,1,1,1,5],[1,1,1,2,2,2],[1,1,1,3,3],[1,1,1,6],[1,1,2,2,3],[1,1,2,5],[1,1,7],[1," +
                "2,2,2,2],[1,2,3,3],[1,2,6],[1,3,5],[2,2,2,3],[2,2,5],[2,7],[3,3,3],[3,6]]");

//
//        /// case 1
//        int[] candidates = new int[]{2, 3, 6, 7};
//        int target = 7;
//        System.out.println("Current: " + combinationSum(candidates, target));
//        System.out.println(" vs Expected: Output: [[2,2,3],[7]]");
//
//        /// case 2
//        candidates = new int[]{2,3,5};
//        target = 8;
//        System.out.println("Current: " + combinationSum(candidates, target));
//        System.out.println("vs Expected: Output: [[2,2,2,2],[2,3,3],[3,5]]");
//
//        /// case 3
//        candidates = new int[]{2};
//        target = 1;
//        System.out.println("Current: " + combinationSum(candidates, target));
//        System.out.println("vs Expected: Output: []");
//
//        /// case 4
//        candidates = new int[]{1};
//        target = 1;
//        System.out.println("Current: " + combinationSum(candidates, target));
//        System.out.println("vs Expected: Output: [1]");
    }

    static final String JOIN_CHAR = "_";

    public static List<List<Integer>> combinationSum(int[] candidates, int target) {
        final Set<String> allSolutions = new HashSet<>();
        findCombination(candidates, target, new HashMap<>(), allSolutions);
        return allSolutions.stream().map(solution -> strToList(solution)).collect(Collectors.toList());
    }

    private static List<Integer> strToList(final String solution) {
        return new ArrayList<>(
                Stream.of(solution.split(JOIN_CHAR)).map(el -> Integer.valueOf(el)).collect(Collectors.toList())
        );
    }

    private static void findCombination(final int[] candidates, final int currSum,
                                        Map<Integer, Integer> currCombination,
                                        Set<String> solutions) {
        log("--findCombination - target:" + currSum + " , current:" + currCombination + " , solutions: " + solutions);
        if (currSum == 0) {
            String currCombinationString = toString(currCombination);
            log("Checking solutions with existing:  " + solutions + " ---> " + currCombinationString);
            if (!solutions.contains(currCombinationString)) {
                log("--- found solution  !!!: " + currCombination);
                solutions.add(currCombinationString);
            } else {
                log("--- solution alreadey included !" + currCombination);
            }
        } else if (currSum > 0) {
            for (int i = 0; i < candidates.length ; i++) {
                if(currSum - candidates[i] < 0){
                    continue;
                }
                log("----checking candidate: " + candidates[i]);
                int candidateCounter = 1;
                if (currCombination.containsKey(candidates[i])) {
                    candidateCounter += currCombination.get(candidates[i]);
                }
                currCombination.put(candidates[i], candidateCounter);
                findCombination(candidates, currSum - candidates[i], currCombination, solutions);
                log("checked - " + currCombination);
                if (candidateCounter == 1) {
                    currCombination.remove(candidates[i]);
                } else {
                    currCombination.put(candidates[i], candidateCounter - 1);
                }
            }
        }
    }

    private static String toString(final Map<Integer, Integer> currCombination) {
        return currCombination.keySet().stream().sorted().flatMapToInt(number ->
                IntStream.range(0, currCombination.get(number)).map(i -> number)
        ).mapToObj(String::valueOf).collect(Collectors.joining(JOIN_CHAR));
    }


}
