package com.horzelam.leetcode.CheckIfTwoStringArraysareEquivalent;

import static org.assertj.core.api.Assertions.assertThat;

public class CheckIfTwoStringArraysareEquivalent {

    public static void main(String[] args) {
        final CheckIfTwoStringArraysareEquivalent m = new CheckIfTwoStringArraysareEquivalent();

        final String[] w1 = new String[]{"ab", "c" };
        final String[] w2 = new String[]{"a", "bc" };
        assertThat(m.arrayStringsAreEqual(w1, w2)).isTrue();
    }

    public boolean arrayStringsAreEqual(String[] word1, String[] word2) {

        return conc(word1).equals(conc(word2));

    }

    private String conc(final String[] word) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < word.length; i++) {
            result.append(word[i]);
        } return result.toString();
    }
}
