package com.horzelam.leetcode.MergeSortedArray;

import static org.assertj.core.api.Assertions.assertThat;

public class MergeSortedArray {
    public static void main(String[] args) {

        final MergeSortedArray main = new MergeSortedArray();
        int[] nums1, nums2;
        int m, n;

        // case 1
        // Input: nums1 = [1,2,3,0,0,0], m = 3, nums2 = [2,5,6], n = 3
        // Output: [1,2,2,3,5,6]
        nums1 = new int[]{1, 2, 3, 0, 0, 0};
        m = 3;
        nums2 = new int[]{2, 5, 6};
        n = 3;
        main.merge(nums1, m, nums2, n);
        assertThat(nums1).containsExactly(1, 2, 2, 3, 5, 6);

        // case 2
        nums1 = new int[]{1};
        m = 1;
        nums2 = new int[]{};
        n = 0;
        main.merge(nums1, m, nums2, n);
        assertThat(nums1).containsExactly(1);

        // case 3
        nums1 = new int[]{0};
        m = 0;
        nums2 = new int[]{1};
        n = 1;
        main.merge(nums1, m, nums2, n);
        assertThat(nums1).containsExactly(1);

    }

    public void merge(int[] nums1, int m, int[] nums2, int n) {

        for (int k = m + n - 1, i = m - 1, j = n - 1; k >= 0; k--) {
            if (j < 0) {
                return;
            } else if (i < 0) {
                nums1[k] = nums2[j];
                j--;
            } else if (nums2[j] > nums1[i]) {
                nums1[k] = nums2[j];
                j--;
            } else {
                nums1[k] = nums1[i];
                i--;
            }
            //System.out.println("took : " + nums1[k] + " and placed @: " + k);
        }

    }
}