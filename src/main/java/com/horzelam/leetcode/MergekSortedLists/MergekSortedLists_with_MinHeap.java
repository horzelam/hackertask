package com.horzelam.leetcode.MergekSortedLists;

import static org.assertj.core.api.Assertions.assertThat;

public class MergekSortedLists_with_MinHeap {
    public static void main(String[] args) {

        MergekSortedLists_with_MinHeap main = new MergekSortedLists_with_MinHeap();
        ListNode[] lists;
        ListNode expected;

        //Input: lists = [[1,4,5],[1,3,4],[2,6]]
        //Output: [1,1,2,3,4,4,5,6]
        lists = new ListNode[]{new ListNode(1, new ListNode(4, new ListNode(5))), new ListNode(1, new ListNode(3, new ListNode(4))),
                new ListNode(2, new ListNode(6))};
        expected = new ListNode(1,
                new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4, new ListNode(4, new ListNode(5, new ListNode(6))))))));
        assertThat(main.mergeKLists(lists)).isEqualToComparingFieldByFieldRecursively(expected);

        // case2 - empty array
        lists = new ListNode[]{};
        expected = null;
        final ListNode res = main.mergeKLists(lists);
        System.out.println(res);
        assertThat(res).isEqualTo(expected);

        //case3 - [[]] -  array with null
        lists = new ListNode[]{null};
        expected = null;
        ListNode res2 = main.mergeKLists(lists);
        assertThat(res2).isEqualTo(expected);
    }

    static class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }

        @Override
        public String toString() {
            return "[" + val + "," + next + "]";
        }
    }

    static class ListNodeCnt {
        ListNode listNode;
        int ind;

        public ListNodeCnt(final ListNode listNode, final int ind) {
            this.listNode = listNode;
            this.ind = ind;
        }
    }

    /**
     * Definition for singly-linked list.
     */
    public ListNode mergeKLists(ListNode[] lists) {
        ListNode outHead = null;
        ListNode outPrev = null;
        boolean isInitialized = false;

        MinHeap minHeap = new MinHeap(lists.length);
        for (int i = 0; i < lists.length; i++) {
            if (lists[i] != null) {
                minHeap.add(new ListNodeCnt(lists[i], i));
            }
        }
        while (minHeap.size > 0) {
            //get current min :
            final ListNodeCnt extracted = minHeap.extractMin();

            // add to output
            final ListNode newOutNode = new ListNode(extracted.listNode.val);
            if (!isInitialized) {
                outHead = newOutNode;
                outPrev = outHead;
                isInitialized = true;
            } else {
                outPrev.next = newOutNode;
                outPrev = outPrev.next;
            }

            // move given list:
            lists[extracted.ind] = lists[extracted.ind].next;

            // add new to heap - if there is anything left
            if (lists[extracted.ind] != null) {
                minHeap.add(new ListNodeCnt(lists[extracted.ind], extracted.ind));
            }

        }
        return outHead;

    }

    class MinHeap {

        private final ListNodeCnt[] heap;
        private int size = 0;

        public MinHeap(int capacity) {
            this.heap = new ListNodeCnt[capacity];
        }

        public ListNodeCnt extractMin() {
            if (size == 0) {
                throw new IllegalStateException("Empty heap !");
            }
            ListNodeCnt extracted = heap[0];

            // replace with largest:
            heap[0] = heap[size - 1];
            size--;
            bubleDown(0);
            return extracted;

        }

        private void bubleDown(int i) {
            ListNodeCnt newVal = heap[i];
            while (i * 2 + 1 < size && (newVal.listNode.val > heap[i * 2 + 1].listNode.val) ||
                    i * 2 + 2 < size && (newVal.listNode.val > heap[i * 2 + 2].listNode.val)) {
                if (newVal.listNode.val > heap[i * 2 + 1].listNode.val &&

                        (i * 2 + 2 == size || heap[i * 2 + 1].listNode.val <= heap[i * 2 + 2].listNode.val)

                ) {
                    heap[i] = heap[i * 2 + 1];
                    heap[i * 2 + 1] = newVal;
                    i = i * 2 + 1;
                } else {
                    heap[i] = heap[i * 2 + 2];
                    heap[i * 2 + 2] = newVal;
                    i = i * 2 + 2;
                }
            }
        }

        public void add(final ListNodeCnt num) {
            if (size == heap.length) {
                throw new IllegalArgumentException("Reached Heap capacity :" + heap.length);
            }

            heap[size] = num;
            fix(size);
            size++;
        }

        private void fix(final int size) {
            int ind = size;
            ListNodeCnt newVal = heap[size];

            while (ind > 0 && newVal.listNode.val < heap[getParent(ind)].listNode.val) {
                heap[ind] = heap[getParent(ind)];
                ind = getParent(ind);
            }
            heap[ind] = newVal;
        }

        private int getParent(final int ind) {
            return (ind - 1) / 2;
        }
    }
}
