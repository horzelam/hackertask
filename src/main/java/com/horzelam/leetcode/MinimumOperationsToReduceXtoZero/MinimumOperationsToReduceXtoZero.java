package com.horzelam.leetcode.MinimumOperationsToReduceXtoZero;

import static org.assertj.core.api.Assertions.assertThat;

public class MinimumOperationsToReduceXtoZero {
    public static void main(String[] args) {
        final MinimumOperationsToReduceXtoZero main = new MinimumOperationsToReduceXtoZero();
        int[] arr;
        int x;

        arr = new int[]{1, 1, 4, 2, 3};
        x = 5;
        assertThat(main.minOperations(arr, x)).isEqualTo(2);

        arr = new int[]{5, 6, 7, 8, 9};
        x = 4;
        assertThat(main.minOperations(arr, x)).isEqualTo(-1);

        arr = new int[]{3, 2, 20, 1, 1, 3};
        x = 10;
        assertThat(main.minOperations(arr, x)).isEqualTo(5);
    }

    public int minOperations(int[] nums, int x) {

        int l = 0;
        int currSum = 0;
        int bestResult = -1;
        for (; l < nums.length && currSum < x; l++) {
            currSum += nums[l];
        }
        if (currSum == x) {
            bestResult = l;
        }
        l--;
        int r = nums.length - 1;
        for (; l >= 0; l--) {
            currSum -= nums[l];
            while (r > l && currSum < x) {
                currSum += nums[r];
                r--;
            }
            if (currSum == x) {
                if (bestResult == -1) {
                    bestResult = l + nums.length - r - 1;
                } else {
                    bestResult = Math.min(bestResult, (l + nums.length - r - 1));
                }
            }
        }
        return bestResult;
    }

    // TOO EXPENSIVE solution

    public int minOperations2(int[] nums, int x) {

        return minOperations2(nums, x, 0, nums.length - 1);

    }

    private int minOperations2(final int[] nums, final int left, final int inds, final int inde) {
        if (left == 0) {
            return 0;
        } else if (left < 0 || inds > inde) {
            return -1;
        } else {
            int leftside = minOperations2(nums, left - nums[inds], inds + 1, inde);
            int rightide = minOperations2(nums, left - nums[inde], inds, inde - 1);
            if (leftside == -1 && rightide == -1) {
                return -1;
            } else if (leftside >= 0 && rightide == -1) {
                return leftside + 1;
            } else if (rightide >= 0 && leftside == -1) {
                return rightide + 1;
            } else {
                if (leftside <= rightide) {
                    return leftside + 1;
                } else {
                    return rightide + 1;
                }
            }
        }
    }
}