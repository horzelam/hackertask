package com.horzelam.leetcode.merge_sorted_lists;

import java.util.StringJoiner;

public class MergeLists {
    /**
     * Definition for singly-linked list.
     */
    static class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }

        @Override
        public String toString() {
            return new StringJoiner(", ",  "[", "]").add("val=" + val).add("next=" + next).toString();
        }
    }

    public static void main(String[] args) {
        final MergeLists b = new MergeLists();

        //Input: l1 = [1,2,4], l2 = [1,3,4]
        //Output: [1,1,2,3,4,4]
        final ListNode l1 = new ListNode(1, new ListNode(2, new ListNode(4)));
        final ListNode l2 = new ListNode(1, new ListNode(3, new ListNode(4)));
        System.out.println(b.mergeTwoLists(l1, l2));
    }

    /**
     * Definition for singly-linked list.
     * public class ListNode {
     * int val;
     * ListNode next;
     * ListNode() {}
     * ListNode(int val) { this.val = val; }
     * ListNode(int val, ListNode next) { this.val = val; this.next = next; }
     * }
     */
    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        ListNode result = new ListNode();

        if (l1 == null ) {
            return l2;
        } else if (l2 == null ) {
            return l1;
        }
        ListNode p1 = l1;
        ListNode p2 = l2;
        ListNode current = null;
        while (p1 != null || p2 != null) {

            int val;
            if (p1 == null ) {
                val = p2.val;
                p2 = p2.next;
            } else if (p2 == null) {
                val = p1.val;
                p1 = p1.next;
            } else {

                if (p1.val <= p2.val) {
                    val = p1.val;
                    p1 = p1.next;
                } else {
                    val = p2.val;
                    p2 = p2.next;
                }
            }


            if(current != null){
                current.next = new ListNode(val);
                current = current.next;
            } else {
                current = new ListNode(val);
                result = current;
            }


        }
        return result;

    }
}