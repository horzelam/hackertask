package com.horzelam.leetcode.SorttheMatrixDiagonally;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class SorttheMatrixDiagonally {
    public static void main(String[] args) {
        final SorttheMatrixDiagonally main = new SorttheMatrixDiagonally();
        int[][] mat, exp;

        //  [[3,3,1,1],[2,2,1,2],[1,1,1,2]]
        mat = new int[][]{new int[]{3, 3, 1, 1}, new int[]{2, 2, 1, 2}, new int[]{1, 1, 1, 2},};
        // [[1,1,1,1],[1,2,2,2],[1,2,3,3]]
        exp = new int[][]{new int[]{1, 1, 1, 1}, new int[]{1, 2, 2, 2}, new int[]{1, 2, 3, 3},};
        assertThat(main.diagonalSort(mat)).isEqualTo(exp);

        //
        mat = new int[][]{new int[]{1}, new int[]{2}, new int[]{4},};
        //
        exp = new int[][]{new int[]{1}, new int[]{2}, new int[]{4},};
        assertThat(main.diagonalSort(mat)).isEqualTo(exp);
    }

    public int[][] diagonalSort(int[][] mat) {

        if (mat.length <= 1 || mat[0].length <= 1) {
            return mat;
        }

        int offset = (mat[0].length - 1);
        int amount = mat.length + mat[0].length - 1;
        List<Integer>[] toSort = new List[amount];

        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[0].length; j++) {
                int diff = i - j;
                if (toSort[diff + offset] == null) {
                    toSort[diff + offset] = new ArrayList<>();
                }
                toSort[diff + offset].add(mat[i][j]);
            }
        }
//        System.out.println("Before:");
//        System.out.println(Arrays.toString(toSort));
        for (int p = 0; p < toSort.length; p++) {
            Collections.sort(toSort[p]);
            final int orgDiff = p - offset;

            int i = orgDiff > 0 ? orgDiff : 0;
            int j = orgDiff < 0 ? -orgDiff : 0;
            for (int k = 0; k < toSort[p].size(); k++) {

                mat[i][j] = toSort[p].get(k);
                i++;
                j++;
            }
        }
//        System.out.println("After:");
//        System.out.println(Arrays.toString(toSort));
        return mat;
    }
}