package com.horzelam.leetcode.LongestSubstringWithoutRepeatingCharacters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class LongestSubstringWithoutRepeatingCharacters {

    public static void main(String[] args) {
        final LongestSubstringWithoutRepeatingCharacters m = new LongestSubstringWithoutRepeatingCharacters();

        assertThat(m.lengthOfLongestSubstring("abcabcbb")).isEqualTo(3);
        assertThat(m.lengthOfLongestSubstring("bbbbb")).isEqualTo(1);
        assertThat(m.lengthOfLongestSubstring("pwwkew")).isEqualTo(3);
        assertThat(m.lengthOfLongestSubstring("")).isEqualTo(0);
        assertThat(m.lengthOfLongestSubstring("aab")).isEqualTo(2);

    }

    public int lengthOfLongestSubstring(String text) {

        int max = 0;
        int currentNonRepeatingSize = 0;
        Map<Character, Integer> nonRepeatingMap = new HashMap<>();

        int posToRemove = -1;
        for (int i = 0, n = text.length(); i < n; i++) {
            final char character = text.charAt(i);

            if (nonRepeatingMap.containsKey(character)) {
                posToRemove = nonRepeatingMap.get(character);
                final int finalPosToRemove = posToRemove;
                final List<Character> toRem = nonRepeatingMap.entrySet()
                        .stream()
                        .parallel()
                        .filter(entry -> entry.getValue() <= finalPosToRemove)
                        .map(en -> en.getKey())
                        .collect(Collectors.toList());

                toRem.forEach(key -> nonRepeatingMap.remove(key));
            }
            nonRepeatingMap.put(character, i);
            currentNonRepeatingSize = i - posToRemove;

            if (currentNonRepeatingSize > max) {
                max = currentNonRepeatingSize;
            }

        }

        return max;
    }

    public int lengthOfLongestSubstring2(String text) {

        int max = 0;
        int currentNonRepeatingSize = 0;
        List<Character> nonRepeatingSub = new ArrayList<>();

        for (int i = 0, n = text.length(); i < n; i++) {
            final char character = text.charAt(i);
            if (nonRepeatingSub.contains(character)) {
                final int subIndex = nonRepeatingSub.indexOf(character);
                nonRepeatingSub = nonRepeatingSub.subList(subIndex + 1, nonRepeatingSub.size());
            }
            nonRepeatingSub.add(character);

            currentNonRepeatingSize = nonRepeatingSub.size();
            if (currentNonRepeatingSize > max) {
                max = currentNonRepeatingSize;
            }

        }

        return max;
    }

}
