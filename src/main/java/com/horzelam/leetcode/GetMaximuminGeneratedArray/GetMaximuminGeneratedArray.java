package com.horzelam.leetcode.GetMaximuminGeneratedArray;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

public class GetMaximuminGeneratedArray {
    public static void main(String[] args) {

        final GetMaximuminGeneratedArray main = new GetMaximuminGeneratedArray();
        main.generateIncreasing(100);
        assertThat(main.getMaximumGenerated(0)).isEqualTo(0);
        assertThat(main.getMaximumGenerated(1)).isEqualTo(1);
        assertThat(main.getMaximumGenerated(2)).isEqualTo(1);
        assertThat(main.getMaximumGenerated(3)).isEqualTo(2);
        assertThat(main.getMaximumGenerated(6)).isEqualTo(3);
        assertThat(main.getMaximumGenerated(7)).isEqualTo(3);
        assertThat(main.getMaximumGenerated(8)).isEqualTo(3);
        assertThat(main.getMaximumGenerated(9)).isEqualTo(4);
        assertThat(main.getMaximumGenerated(10)).isEqualTo(4);
        assertThat(main.getMaximumGenerated(11)).isEqualTo(5);
        assertThat(main.getMaximumGenerated(12)).isEqualTo(5);
        assertThat(main.getMaximumGenerated(13)).isEqualTo(5);
        assertThat(main.getMaximumGenerated(14)).isEqualTo(5);

        assertThat(main.getMaximumGenerated(15)).isEqualTo(5); // expected 5

        assertThat(main.getMaximumGenerated(16)).isEqualTo(5);
        assertThat(main.getMaximumGenerated(25)).isEqualTo(8);

        assertThat(main.getMaximumGenerated(30)).isEqualTo(8);

    }
    // n = 0...
    public int getMaximumGenerated(int n) {
        if (n < 2) {
            return n;
        }else {
            return generateIncreasing(n);
        }
    }

    public int generateIncreasing(int n) {
        final int[] arr = new int[n + 1];
        arr[0]=0;
        arr[1]=1;
        int max=1;
        for (int i = 2;i<n+1;i++){
            if(i%2==0)
                arr[i]=arr[i/2];
            else
                arr[i]=arr[(i-1)/2] + arr[(i-1)/2 + 1];
            if(arr[i]>max)
                max = arr[i];
        }
        //System.out.println(Arrays.toString(arr));
        return max;
    }

    // n = 0...
    public int getMaximumGeneratedOld(int n) {
        if (n < 2) {
            return n;
        }
        if (n % 2 == 0) {
            int[] arr = generateArr(n);
            if (n > 3) {
                generate(n - 3, arr);
                System.out.println(Arrays.toString(arr) + "				 n:" + n + " taking n-1 element from");
                return Math.max(arr[n - 1], arr[n - 3]);
            } else {
                System.out.println(Arrays.toString(arr) + "				 n:" + n + " taking n-1 element from");
                return arr[n - 1];
            }
        } else {
            int[] arr = generateArr(n + 1);
            generate(n - 2, arr);
            System.out.println(Arrays.toString(arr) + "				 n:" + n + " taking n element from");
            return Math.max(arr[n], arr[n - 2]);
        }
    }

    // n = 1...
    public int[] generateArr(int size) {
        final int[] arr = new int[size];
        generate(size - 1, arr);
        return arr;
    }

    public int generate(int i, int[] nums) {
        if (i == 0) {
            nums[1] = 0;
        } else if (i == 1) {
            nums[1] = 1;
        } else {
            if (nums[i] == 0) {
                if (i % 2 == 0) {
                    nums[i] = generate(i / 2, nums);
                } else {
                    int prevInd = (i - 1) / 2;
                    nums[i] = generate(prevInd, nums) + generate(prevInd + 1, nums);
                    //nums[2 * i + 1] = nums[i] + nums[i + 1] when 2 <= 2 * i + 1 <= n

                }
            }
        }
        return nums[i];

    }
}
