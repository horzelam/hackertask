package com.horzelam.leetcode.KthLargestElementinanArray;

import static org.assertj.core.api.Assertions.assertThat;

public class KthLargestElementinanArrayMaxHeap {
    public static void main(String[] args) {
        final KthLargestElementinanArrayMaxHeap main = new KthLargestElementinanArrayMaxHeap();
        int[] arr;
        int k;

        //        // Example 1:
                arr = new int[]{3, 2, 1, 5, 6, 4};
                k = 2;
                assertThat(main.findKthLargest(arr, k)).isEqualTo(5);

                //Example 2:
                arr = new int[]{3, 2, 3, 1, 2, 4, 5, 5, 6};
                k = 4;
                assertThat(main.findKthLargest(arr, k)).isEqualTo(4);

                arr = new int[]{3, 1, 2, 4};
                k = 2;
                assertThat(main.findKthLargest(arr, k)).isEqualTo(3);

        arr = new int[]{2, 1};
        k = 1;
        assertThat(main.findKthLargest(arr, k)).isEqualTo(2);
    }

    public int findKthLargest(int[] nums, int k) {

        final MaxHeap heap = buildMaxHeap(nums);
        int val = 0;
        while (k > 0) {
            val = heap.extractMax();
            k--;
        }
        return val;
    }

    private MaxHeap buildMaxHeap(final int[] nums) {
        MaxHeap heap = new MaxHeap(nums.length);
        for (int i = 0; i < nums.length; i++) {
            heap.add(nums[i]);
        }
        return heap;
    }

    class MaxHeap {

        private final int[] heap;
        private int size = 0;

        public MaxHeap(int capacity) {
            this.heap = new int[capacity];
        }

        public int extractMax() {
            if (size == 0) {
                throw new IllegalStateException("Empty heap !");
            }
            int extracted = heap[0];

            // replace with smallest:
            heap[0] = heap[size - 1];
            size--;
            bubleDown(0);
            return extracted;

        }

        private void bubleDown(int i) {
            int newVal = heap[i];
            while (
                    i * 2 + 1 < size && (newVal < heap[i * 2 + 1])
                ||
                    i * 2 + 2 < size && (newVal < heap[i * 2 + 2])
            ) {
                if (newVal < heap[i * 2 + 1] &&

                        (   i * 2 + 2 == size ||
                            heap[i * 2 + 1] >= heap[i * 2 + 2]
                        )

                ) {
                    heap[i] = heap[i * 2 + 1];
                    heap[i * 2 + 1] = newVal;
                    i = i * 2 + 1;
                } else {
                    heap[i] = heap[i * 2 + 2];
                    heap[i * 2 + 2] = newVal;
                    i = i * 2 + 2;
                }
            }
        }

        public void add(final int num) {
            if (size == heap.length) {
                throw new IllegalArgumentException("Reached Heap capacity :" + heap.length);
            }

            heap[size] = num;
            fix(size);
            size++;
        }

        private void fix(final int size) {
            int ind = size;
            int newVal = heap[size];

            while (ind > 0 && newVal > heap[getParent(ind)]) {
                heap[ind] = heap[getParent(ind)];
                ind = getParent(ind);
            }
            heap[ind] = newVal;
        }

        private int getParent(final int ind) {
            return (ind - 1) / 2;
        }
    }

}
