package com.horzelam.leetcode.KthLargestElementinanArray;

import java.util.Arrays;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

public class KthLargestElementinanArray {
    public static void main(String[] args) {
        final KthLargestElementinanArray main = new KthLargestElementinanArray();
        int[] arr;
        int k;

        // Example 1:
        arr = new int[]{3, 2, 1, 5, 6, 4};
        k = 2;
        assertThat(main.findKthLargest(arr, k)).isEqualTo(5);

        //Example 2:
        arr = new int[]{3, 2, 3, 1, 2, 4, 5, 5, 6};
        k = 4;
        assertThat(main.findKthLargest(arr, k)).isEqualTo(4);

    }

    public int findKthLargest(int[] nums, int k) {

        Arrays.sort(nums);
        return nums[nums.length-k];
//        final int[] largest = new int[k];
//        int foundLargest = 0;
//        for (int i = 0; i < nums.length; i++) {
//
//            nums[i]
//        }
//        return 0;
    }
}
