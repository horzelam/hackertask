package com.horzelam.leetcode;

import java.util.*;

/**
 * Given an array of distinct integers candidates and a target integer target,
 * return a list of all unique combinations of candidates where the chosen numbers sum to target. You may return the
 * combinations in any order.
 * <p>
 * The same number may be chosen from candidates an unlimited number of times.
 * Two combinations are unique if the frequency of at least one of the chosen numbers is different.
 * <p>
 * It is guaranteed that the number of unique combinations that sum up to target is less than 150 combinations for
 * the given input.
 */
public class CombinationSum8Best {
//
//    private static void log(final String s) {
//        System.out.println("....." + s);
//    }

    public static void main(String[] args) {
        /// case 1
        int[] candidates = new int[]{2, 7, 6, 3, 5, 1};
        int target = 9;
        Long start = System.currentTimeMillis();
        System.out.println("Current: " + combinationSum(candidates, target) + " -- " + (System.currentTimeMillis() - start) + " ms");
        System.out.println(" vs Expected: Output: [[1,1,1,1,1,1,1,1,1],[1,1,1,1,1,1,1,2],[1,1,1,1,1,1,3],[1,1,1,1,1," +
                "2,2],[1,1,1,1,2,3],[1,1,1,1,5],[1,1,1,2,2,2],[1,1,1,3,3],[1,1,1,6],[1,1,2,2,3],[1,1,2,5],[1,1,7],[1," +
                "2,2,2,2],[1,2,3,3],[1,2,6],[1,3,5],[2,2,2,3],[2,2,5],[2,7],[3,3,3],[3,6]]");

//
//        /// case 1
//        int[] candidates = new int[]{2, 3, 6, 7};
//        int target = 7;
//        System.out.println("Current: " + combinationSum(candidates, target));
//        System.out.println(" vs Expected: Output: [[2,2,3],[7]]");
//
//        /// case 2
//        candidates = new int[]{2,3,5};
//        target = 8;
//        System.out.println("Current: " + combinationSum(candidates, target));
//        System.out.println("vs Expected: Output: [[2,2,2,2],[2,3,3],[3,5]]");
//
//        /// case 3
//        candidates = new int[]{2};
//        target = 1;
//        System.out.println("Current: " + combinationSum(candidates, target));
//        System.out.println("vs Expected: Output: []");
//
//        /// case 4
//        candidates = new int[]{1};
//        target = 1;
//        System.out.println("Current: " + combinationSum(candidates, target));
//        System.out.println("vs Expected: Output: [1]");
    }

    public static List<List<Integer>> combinationSum(int[] candidates, int target) {
        Set<List<Integer>> allSolutions = new HashSet<>();
        Arrays.sort(candidates);
        findCombination(candidates, target, new ArrayList<>(),0, allSolutions);
        return new ArrayList<>(allSolutions);
    }


    private static void findCombination(final int[] candidates, final int target,
                                        List<Integer> currCombination, int i, Set<List<Integer>> allSolutions) {
        //System.out.println("--check" + currCombination);
        if (target == 0) {
            //Collections.sort(currCombination);
            allSolutions.add(currCombination);
        } else {
            for (; i < candidates.length && target - candidates[i] >= 0; i++) {
                final ArrayList<Integer> newCombination = new ArrayList<>(currCombination);
                newCombination.add(candidates[i]);
                findCombination(candidates, target - candidates[i], newCombination, i, allSolutions);
                //log("checked - " + newCombination);
                i = i++;
            }
        }
    }
}
