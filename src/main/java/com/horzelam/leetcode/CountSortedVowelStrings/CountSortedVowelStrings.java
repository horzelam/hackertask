package com.horzelam.leetcode.CountSortedVowelStrings;

import static org.assertj.core.api.Assertions.assertThat;

public class CountSortedVowelStrings {
    public static void main(String[] args) {
        // Input: n = 2
        //Output: 15
        //Explanation: The 15 sorted strings that consist of vowels only are
        //["aa","ae","ai","ao","au","ee","ei","eo","eu","ii","io","iu","oo","ou","uu"].
        //Note that "ea" is not a valid string since 'e' comes after 'a' in the alphabet.

        final CountSortedVowelStrings main = new CountSortedVowelStrings();

        assertThat(main.countVowelStrings(1)).isEqualTo(5);
        assertThat(main.countVowelStrings(2)).isEqualTo(15);
        assertThat(main.countVowelStrings(3)).isEqualTo(35);
        assertThat(main.countVowelStrings(33)).isEqualTo(66045);

    }

    public int countVowelStrings(int n) {
        int total = 5;
        int prevA = 1;
        int prevE = 1;
        int prevI = 1;
        int prevO = 1;

        for (int i = 1; i < n; i++) {
            int nextA = total;
            int nextE = total - prevA;
            int nextI = nextE - prevE;
            int nextO = nextI - prevI;
            int nextU = nextO - prevO;
            total = nextA + nextE + nextI + nextO + nextU;
            prevA = nextA;
            prevE = nextE;
            prevI = nextI;
            prevO = nextO;
        }
        return total;
    }
}
