package com.horzelam.leetcode.MaxNumberofKSumPairs;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class MaxNumberofKSumPairs {
    public static void main(String[] args) {
        final MaxNumberofKSumPairs main = new MaxNumberofKSumPairs();
        //Input: nums = [1,2,3,4], k = 5
        //Output: 2
        int[] nums;
        int k;
        // case 1
        nums = new int[]{1, 2, 3, 4};
        k = 5;
        assertThat(main.maxOperations(nums, k)).isEqualTo(2);

        // case 2
        nums = new int[]{3, 1, 3, 4, 3};
        k = 6;
        assertThat(main.maxOperations(nums, k)).isEqualTo(1);

    }

    public int maxOperations(int[] nums, int k) {
        Map<Integer, Integer> map = new HashMap(nums.length);
        int pairs = 0;
        for (int i = 0; i < nums.length; i++) {
            int second = k - nums[i];
            if (map.containsKey(second)) {
                final Integer amount = map.get(second);
                if (amount > 1) {
                    map.put(second, amount - 1);
                } else {
                    map.remove(second);
                }
                pairs++;
            } else {
                int val = 1;
                if (map.containsKey(nums[i])) {
                    val += map.get(nums[i]);
                }
                map.put(nums[i], val);
            }
        }
        return pairs;

    }
}