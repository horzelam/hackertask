package com.horzelam.leetcode.removeDuplicatesFromLists;

import java.util.StringJoiner;

public class RemoveDuplicatesFromListsII {
    //Remove Duplicates from Sorted List II

    public static void main(String[] args) {
        final RemoveDuplicatesFromListsII b = new RemoveDuplicatesFromListsII();

        //final ListNode l1 = new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(3, new ListNode(4, new ListNode(5))))));
        //[1,2,3,3,4,4,5]
        final ListNode l1 =
                new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(3, new ListNode(4, new ListNode(4, new ListNode(5)))))));
        System.out.println(b.deleteDuplicates(l1));
    }

    static class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }

        @Override
        public String toString() {
            return new StringJoiner(", ", "[", "]").add("val=" + val).add("next=" + next).toString();
        }

    }

    public ListNode deleteDuplicates(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }

        ListNode lastCreated = null;
        ListNode resultHead = null;

        ListNode currentExplored = head;
        boolean wasDuplicated = false;

        while (currentExplored != null) {

            Integer value = null;
            if(!wasDuplicated){
                if(currentExplored.next == null || currentExplored.next.val != currentExplored.val){
                    value = currentExplored.val;
                } else {
                    wasDuplicated = true;
                }
            } else {
                if(currentExplored.next != null && currentExplored.next.val != currentExplored.val){
                    wasDuplicated = false;
                }
            }


//            if (!wasDuplicated && (currentExplored.next.val != currentExplored.val || currentExplored.next == null)) {
//                value = currentExplored.val;
//            } else if (!wasDuplicated) {
//                wasDuplicated = true;
//            } else if (currentExplored.next.val != currentExplored.val) {
//                wasDuplicated = false;
//            } else {
//                // was duplicated previously, and new value is the same
//            }
            currentExplored = currentExplored.next;

            if (value != null) {
                if (resultHead == null) {
                    resultHead = new ListNode(value);
                    lastCreated = resultHead;
                } else {
                    ListNode newNode = new ListNode(value);
                    lastCreated.next = newNode;
                    lastCreated = newNode;
                }
            }
        }
        return resultHead;
    }
}
