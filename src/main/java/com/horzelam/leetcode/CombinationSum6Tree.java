package com.horzelam.leetcode;

import java.util.*;

/**
 * Given an array of distinct integers candidates and a target integer target,
 * return a list of all unique combinations of candidates where the chosen numbers sum to target. You may return the
 * combinations in any order.
 * <p>
 * The same number may be chosen from candidates an unlimited number of times.
 * Two combinations are unique if the frequency of at least one of the chosen numbers is different.
 * <p>
 * It is guaranteed that the number of unique combinations that sum up to target is less than 150 combinations for
 * the given input.
 */
public class CombinationSum6Tree {

    //private static Node root;

    public static void PrintTree(Node tree, String indent, boolean last) {
        System.out.println(indent + "+- [" + tree.value + ", "+ tree.currTarget+"]");
        indent += last ? "   " : "|  ";

        for (int i = 0; i < tree.list.size(); i++) {
            PrintTree(tree.list.get(i), indent, i == tree.list.size() - 1);
        }
    }



//
//    private static void log(final String s) {
//        System.out.println("....." + s);
//    }

    public static void main(String[] args) {
//        /// case 1
        int[] candidates = new int[]{2, 7, 6, 3, 5, 1};
        int target = 9;
        System.out.println("    Curr: " + combinationSum(candidates, target));
        System.out.println(" vs Expt: [[1, 1, 1, 1, 1, 1, 1, 1, 1], [1, 1, 1, 1, 1, 1, 1, 2], [1, 1, 1, 1, 1, 1, 3], [1, 1, 1, 1, 1, 2, 2], [1, 1, 1, 1, 2, 3], [1, 1, 1, 1, 5], [1, 1, 1, 2, 2, 2], [1, 1, 1, 3, 3], [1, 1, 1, 6], [1, 1, 2, 2, 3], [1, 1, 2, 5], [1, 1, 7], [1, 2, 2, 2, 2], [1, 2, 3, 3], [1, 2, 6], [1, 3, 5], [2, 2, 2, 3], [2, 2, 5], [2, 7], [3, 3, 3], [3, 6]]");

//        int[] candidates = new int[]{ 2, 3, 1};
//        int target = 3;
//        System.out.println("Current: " + combinationSum(candidates, target));
//        System.out.println(" vs Expected: Output: [[1,1,1],[1,2],[3]]");

//
//        /// case 1
//        int[] candidates = new int[]{2, 3, 6, 7};
//        int target = 7;
//        System.out.println("Current: " + combinationSum(candidates, target));
//        System.out.println(" vs Expected: Output: [[2,2,3],[7]]");

//        /// case 2
//        int[] candidates = new int[]{2,3,5};
//        int target = 8;
//        System.out.println("Current: " + combinationSum(candidates, target));
//        System.out.println("vs Expected: Output: [[2,2,2,2],[2,3,3],[3,5]]");
//
//        /// case 3
//        candidates = new int[]{2};
//        target = 1;
//        System.out.println("Current: " + combinationSum(candidates, target));
//        System.out.println("vs Expected: Output: []");
//
//        /// case 4
//        candidates = new int[]{1};
//        target = 1;
//        System.out.println("Current: " + combinationSum(candidates, target));
//        System.out.println("vs Expected: Output: [1]");
    }

    public static List<List<Integer>> combinationSum(int[] candidates, int target) {
        Node root = new Node(0, target);
        Arrays.sort(candidates);
        final Map<Integer, Node> cache = new HashMap<>();
        findCombination(candidates, target, root, cache);

        PrintTree(root, "", true);


        final Set<List<Integer>> solutionsSet =new HashSet<>();
        extractSolutions(root, new ArrayList<>(), solutionsSet);

        return new ArrayList<>(solutionsSet);
    }

    private static void extractSolutions(final Node node, final List<Integer> currSolution,
                                         final Set<List<Integer>> solutions) {
        if (node.currTarget == 0) {
            Collections.sort(currSolution);
            solutions.add(currSolution);
        } else {
            if (node.ref != null) {
                 node.ref.list.forEach(child -> {
                    final List<Integer> newSolution = new ArrayList<>(currSolution);
                    newSolution.add(child.value);
                    extractSolutions(child, newSolution, solutions);
                });
            } else {
                node.list.forEach(child -> {
                    final List<Integer> newSolution = new ArrayList<>(currSolution);
                    newSolution.add(child.value);
                    extractSolutions(child, newSolution, solutions);
                });
            }
        }
    }


    private static void findCombination(final int[] candidates, final int target,
                                        Node node, Map<Integer, Node> cache) {

        if (target > 0) {
            for (int i = 0; i < candidates.length && target - candidates[i] >= 0; i++) {
                final int newTargetValue = target - candidates[i];


                // if there exists any Node with the same newTargetValue ?

                // if not :
                if (newTargetValue > 0 && cache.containsKey(newTargetValue)) {
                    //System.out.println("For node: ["+ candidates[i] + ", "+ newTargetValue + "] found cache:" + newTargetValue + " - > " + cache.get(newTargetValue));
                    final Node child = new Node(candidates[i], newTargetValue);
                    child.setRef(cache.get(newTargetValue));
                    node.addNode(child);
                } else {
                    final Node child = new Node(candidates[i], newTargetValue);
                    node.addNode(child);
                    cache.put(newTargetValue, child);
                    //System.out.println("..Exploring:" + candidates[i] + " -- curr tree: ");
                    //PrintTree(root, "", true);
                    findCombination(candidates, newTargetValue, child, cache);
                }
                //log("checked - " + newCombination);
            }
        }
    }


    static class Node {
        int value = 0;
        int currTarget = 0;
        List<Node> list = new ArrayList<>();
        // sibling node - having the same currTarget , so the same children
        Node ref;

        public Node(final int value, final int target) {
            this.value = value;
            this.currTarget = target;
        }


        public void addNode(final Node nodeChild) {
            this.list.add(nodeChild);
        }

        public void setRef(final Node node) {
            ref = node;
        }
    }
}
