package com.horzelam.leetcode.WordLadder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class WordLadderDFS {

    public static void main(String[] args) {
        final WordLadderDFS b = new WordLadderDFS();

        //                        final String begin = "hit";
        //                        final String end = "cog";
        //                        List<String> wordList = Arrays.asList("hot", "dot", "dog", "lot", "log", "cog");
        //                        // "hit" -> "hot" -> "dot" -> "dog" -> "cog"
        //                        System.out.println(b.ladderLength(begin, end, wordList));
        //                        assertThat(b.ladderLength(begin, end, wordList)).isEqualTo(5);

        //
        //Input: beginWord = "hit", endWord = "cog", wordList = ["hot","dot","dog","lot","log"]
        //Output: 0
        //Explanation: The endWord "cog" is not in wordList, therefore no possible transformation.
        //                List<String> wordList = Arrays.asList("hot","dot","dog","lot","log");
        //                String begin = "hit";
        //                String end = "cog";
        //                System.out.println(b.ladderLength(begin, end, wordList));
        //                        assertThat(b.ladderLength(begin, end, wordList)).isEqualTo(0);

        //        String begin = "qa";
        //        String end = "sq";
        //        List<String> wordList =
        //                Arrays.asList("si", "go", "se", "cm", "so", "ph", "mt", "db", "mb", "sb", "kr", "ln", "tm", "le", "av", "sm", "ar", "ci",
        //                        "ca", "br", "ti", "ba", "to", "ra", "fa", "yo", "ow", "sn", "ya", "cr", "po", "fe", "ho", "ma", "re", "or", "rn",
        //                        "au", "ur", "rh", "sr", "tc", "lt", "lo", "as", "fr", "nb", "yb", "if", "pb", "ge", "th", "pm", "rb", "sh", "co",
        //                        "ga", "li", "ha", "hz", "no", "bi", "di", "hi", "qa", "pi", "os", "uh", "wm", "an", "me", "mo", "na", "la", "st",
        //                        "er", "sc", "ne", "mn", "mi", "am", "ex", "pt", "io", "be", "fm", "ta", "tb", "ni", "mr", "pa", "he", "lr", "sq",
        //                        "ye");
        //        System.out.println(b.ladderLength(begin, end, wordList));
        //        assertThat(b.ladderLength(begin, end, wordList)).isEqualTo(5);

        String begin = "cet";
        String end = "ism";
        List<String> wordList =
                Arrays.asList("kid", "tag", "pup", "ail", "tun", "woo", "erg", "luz", "brr", "gay", "sip", "kay", "per", "val", "mes",
                        "ohs", "now", "boa", "cet", "pal", "bar", "die", "war", "hay", "eco", "pub", "lob", "rue", "fry", "lit", "rex",
                        "jan", "cot", "bid", "ali", "pay", "col", "gum", "ger", "row", "won", "dan", "rum", "fad", "tut", "sag", "yip",
                        "sui", "ark", "has", "zip", "fez", "own", "ump", "dis", "ads", "max", "jaw", "out", "btu", "ana", "gap", "cry",
                        "led", "abe", "box", "ore", "pig", "fie", "toy", "fat", "cal", "lie", "noh", "sew", "ono", "tam", "flu", "mgm",
                        "ply", "awe", "pry", "tit", "tie", "yet", "too", "tax", "jim", "san", "pan", "map", "ski", "ova", "wed", "non",
                        "wac", "nut", "why", "bye", "lye", "oct", "old", "fin", "feb", "chi", "sap", "owl", "log", "tod", "dot", "bow",
                        "fob", "for", "joe", "ivy", "fan", "age", "fax", "hip", "jib", "mel", "hus", "sob", "ifs", "tab", "ara", "dab",
                        "jag", "jar", "arm", "lot", "tom", "sax", "tex", "yum", "pei", "wen", "wry", "ire", "irk", "far", "mew", "wit",
                        "doe", "gas", "rte", "ian", "pot", "ask", "wag", "hag", "amy", "nag", "ron", "soy", "gin", "don", "tug", "fay",
                        "vic", "boo", "nam", "ave", "buy", "sop", "but", "orb", "fen", "paw", "his", "sub", "bob", "yea", "oft", "inn",
                        "rod", "yam", "pew", "web", "hod", "hun", "gyp", "wei", "wis", "rob", "gad", "pie", "mon", "dog", "bib", "rub",
                        "ere", "dig", "era", "cat", "fox", "bee", "mod", "day", "apr", "vie", "nev", "jam", "pam", "new", "aye", "ani",
                        "and", "ibm", "yap", "can", "pyx", "tar", "kin", "fog", "hum", "pip", "cup", "dye", "lyx", "jog", "nun", "par",
                        "wan", "fey", "bus", "oak", "bad", "ats", "set", "qom", "vat", "eat", "pus", "rev", "axe", "ion", "six", "ila",
                        "lao", "mom", "mas", "pro", "few", "opt", "poe", "art", "ash", "oar", "cap", "lop", "may", "shy", "rid", "bat",
                        "sum", "rim", "fee", "bmw", "sky", "maj", "hue", "thy", "ava", "rap", "den", "fla", "auk", "cox", "ibo", "hey",
                        "saw", "vim", "sec", "ltd", "you", "its", "tat", "dew", "eva", "tog", "ram", "let", "see", "zit", "maw", "nix",
                        "ate", "gig", "rep", "owe", "ind", "hog", "eve", "sam", "zoo", "any", "dow", "cod", "bed", "vet", "ham", "sis",
                        "hex", "via", "fir", "nod", "mao", "aug", "mum", "hoe", "bah", "hal", "keg", "hew", "zed", "tow", "gog", "ass",
                        "dem", "who", "bet", "gos", "son", "ear", "spy", "kit", "boy", "due", "sen", "oaf", "mix", "hep", "fur", "ada",
                        "bin", "nil", "mia", "ewe", "hit", "fix", "sad", "rib", "eye", "hop", "haw", "wax", "mid", "tad", "ken", "wad",
                        "rye", "pap", "bog", "gut", "ito", "woe", "our", "ado", "sin", "mad", "ray", "hon", "roy", "dip", "hen", "iva",
                        "lug", "asp", "hui", "yak", "bay", "poi", "yep", "bun", "try", "lad", "elm", "nat", "wyo", "gym", "dug", "toe",
                        "dee", "wig", "sly", "rip", "geo", "cog", "pas", "zen", "odd", "nan", "lay", "pod", "fit", "hem", "joy", "bum",
                        "rio", "yon", "dec", "leg", "put", "sue", "dim", "pet", "yaw", "nub", "bit", "bur", "sid", "sun", "oil", "red",
                        "doc", "moe", "caw", "eel", "dix", "cub", "end", "gem", "off", "yew", "hug", "pop", "tub", "sgt", "lid", "pun",
                        "ton", "sol", "din", "yup", "jab", "pea", "bug", "gag", "mil", "jig", "hub", "low", "did", "tin", "get", "gte",
                        "sox", "lei", "mig", "fig", "lon", "use", "ban", "flo", "nov", "jut", "bag", "mir", "sty", "lap", "two", "ins",
                        "con", "ant", "net", "tux", "ode", "stu", "mug", "cad", "nap", "gun", "fop", "tot", "sow", "sal", "sic", "ted",
                        "wot", "del", "imp", "cob", "way", "ann", "tan", "mci", "job", "wet", "ism", "err", "him", "all", "pad", "hah",
                        "hie", "aim", "ike", "jed", "ego", "mac", "baa", "min", "com", "ill", "was", "cab", "ago", "ina", "big", "ilk",
                        "gal", "tap", "duh", "ola", "ran", "lab", "top", "gob", "hot", "ora", "tia", "kip", "han", "met", "hut", "she",
                        "sac", "fed", "goo", "tee", "ell", "not", "act", "gil", "rut", "ala", "ape", "rig", "cid", "god", "duo", "lin",
                        "aid", "gel", "awl", "lag", "elf", "liz", "ref", "aha", "fib", "oho", "tho", "her", "nor", "ace", "adz", "fun",
                        "ned", "coo", "win", "tao", "coy", "van", "man", "pit", "guy", "foe", "hid", "mai", "sup", "jay", "hob", "mow",
                        "jot", "are", "pol", "arc", "lax", "aft", "alb", "len", "air", "pug", "pox", "vow", "got", "meg", "zoe", "amp",
                        "ale", "bud", "gee", "pin", "dun", "pat", "ten", "mob");
        System.out.println(b.ladderLength(begin, end, wordList));
        assertThat(b.ladderLength(begin, end, wordList)).isEqualTo(5);
    }

    public int ladderLength(String beginWord, String endWord, List<String> wordList) {

        Map<String, Integer> indexMap = prepareIndexMap(wordList, beginWord);
        if (!indexMap.containsKey(endWord)) {
            return 0;
        }
//        System.out.println("prepared map: " + indexMap);
        Map<Integer, List<Integer>> siblingsMap = prepareSiblings(indexMap);
//        System.out.println("Siblings map: " + siblingsMap);
//        System.out.println("----  prepared maps ----");

        final List<Integer> visited = new ArrayList<>();
        visited.add(indexMap.get(beginWord));

        final AtomicInteger shortest = new AtomicInteger(Integer.MAX_VALUE);
        dfs(indexMap.get(beginWord), indexMap.get(endWord), siblingsMap, visited, shortest);
        return shortest.get() == Integer.MAX_VALUE ? 0 : shortest.get();

    }

    private Map<String, Integer> prepareIndexMap(final List<String> wordList, final String beginWord) {
        final Map<String, Integer> map = new HashMap<>();
        int i = 0;
        for (; i < wordList.size(); i++) {
            map.put(wordList.get(i), i);
        }
        if (!map.containsKey(beginWord)) {
            map.put(beginWord, i++);
        }
        return map;
    }

    private Character[] alphabet =
            new Character[]{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                    'w', 'x', 'y', 'z' };

    private Map<Integer, List<Integer>> prepareSiblings(final Map<String, Integer> indexMap) {
        final Map<Integer, List<Integer>> result = new HashMap<>();
        for (String word : indexMap.keySet()) {
            final Integer originalInd = indexMap.get(word);
            final List<Integer> siblings = new ArrayList<>();
            final char[] wordArr = word.toCharArray();
            for (Character charc : alphabet) {
                // given alhpabet letter
                // generate sibling Candidates , replacing i-th character with given letter
                for (int i = 0; i < word.length(); i++) {
                    final char oldChar = wordArr[i];
                    wordArr[i] = charc;
                    String siblingCandidate = new String(wordArr);
                    //System.out.println("Checking candidate: " + siblingCandidate);
                    if (indexMap.containsKey(siblingCandidate) && indexMap.get(siblingCandidate) != originalInd) {
                        siblings.add(indexMap.get(siblingCandidate));
                    }
                    wordArr[i] = oldChar;

                }

            }
            //System.out.println("For " + original + " checking siblings: " + siblings);
            result.put(originalInd, siblings);
        }
        return result;
    }

    void dfs(int current, int end, Map<Integer, List<Integer>> siblingsMap, List<Integer> visited, final AtomicInteger shortestFound) {
        // System.out.println("--bfs for " + current + " with visited " + visited);
        if (current == end) {
            //System.out.println("--- !!! reached end: " + visited);
            if (visited.size() < shortestFound.get()) {
                shortestFound.set(visited.size());
            }
            return;

        } else {
            if (visited.size() == siblingsMap.keySet().size() || visited.size() > shortestFound.get()) {
                return;
            }

            List<Integer> similarList =
                    siblingsMap.get(current).stream().filter(item -> !visited.contains(item)).collect(Collectors.toList());

            //  System.out.println("----for " + current + " - found similar to visit: " + similarList +" curr visited: " + visited);
            for (Integer similar : similarList) {

                visited.add(similar);
                final int toRemoveInd = visited.size() - 1;
                dfs(similar, end, siblingsMap, visited, shortestFound);
                visited.remove(toRemoveInd);

            }
        }
    }

}
