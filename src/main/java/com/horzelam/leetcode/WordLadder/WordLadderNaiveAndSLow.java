package com.horzelam.leetcode.WordLadder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class WordLadderNaiveAndSLow {

    public static void main(String[] args) {
        final WordLadderNaiveAndSLow b = new WordLadderNaiveAndSLow();

        //        final String begin = "hit";
        //        final String end = "cog";
        //        List<String> wordList = Arrays.asList("hot", "dot", "dog", "lot", "log", "cog");
        //        // "hit" -> "hot" -> "dot" -> "dog" -> "cog"
        //        System.out.println(b.ladderLength(begin, end, wordList));
        //        assertThat(b.ladderLength(begin, end, wordList)).isEqualTo(5);
        //
        //
        //        //Input: beginWord = "hit", endWord = "cog", wordList = ["hot","dot","dog","lot","log"]
        //        //Output: 0
        //        //Explanation: The endWord "cog" is not in wordList, therefore no possible transformation.
        //        wordList = Arrays.asList("hot","dot","dog","lot","log");
        //        System.out.println(b.ladderLength(begin, end, wordList));
        //        assertThat(b.ladderLength(begin, end, wordList)).isEqualTo(0);

        final String begin = "qa";
        final String end = "sq";
        List<String> wordList =
                Arrays.asList("si", "go", "se", "cm", "so", "ph", "mt", "db", "mb", "sb", "kr", "ln", "tm", "le", "av", "sm", "ar", "ci",
                        "ca", "br", "ti", "ba", "to", "ra", "fa", "yo", "ow", "sn", "ya", "cr", "po", "fe", "ho", "ma", "re", "or", "rn",
                        "au", "ur", "rh", "sr", "tc", "lt", "lo", "as", "fr", "nb", "yb", "if", "pb", "ge", "th", "pm", "rb", "sh", "co",
                        "ga", "li", "ha", "hz", "no", "bi", "di", "hi", "qa", "pi", "os", "uh", "wm", "an", "me", "mo", "na", "la", "st",
                        "er", "sc", "ne", "mn", "mi", "am", "ex", "pt", "io", "be", "fm", "ta", "tb", "ni", "mr", "pa", "he", "lr", "sq",
                        "ye");
        System.out.println(b.ladderLength(begin, end, wordList));
        assertThat(b.ladderLength(begin, end, wordList)).isEqualTo(5);

    }

    public int ladderLength(String beginWord, String endWord, List<String> wordList) {
        List<String> visited = new ArrayList();
        visited.add(beginWord);
        final List<String> toVisit = new ArrayList<>();
        wordList.stream().filter(item -> !item.equals(beginWord)).forEach(item -> toVisit.add(item));
        return bfs(beginWord, endWord, visited, toVisit, wordList.size());
    }

    int bfs(String current, String end, List<String> visited, List<String> toVisit, int allSize) {
        //System.out.println("--bfs for " + current + " with visited " + visited);
        if (current.equals(end)) {
            return visited.size();
        } else {
            if (visited.size() == allSize) {
                return 0;
            }

            int bfsFoundMin = 0;
            List<String> similarList = findSimilar(current, visited, toVisit);
            //System.out.println("----for " + current + " - found similar: " + similarList +" curr visited: " + visited);
            for (String similar : similarList) {
                visited.add(similar);
                toVisit.remove(similar);
                final int toRemoveInd = visited.size() - 1;
                final int bfsFound = bfs(similar, end, visited, toVisit, allSize);
                if (bfsFound > 0 && (bfsFoundMin == 0 || bfsFound < bfsFoundMin)) {
                    bfsFoundMin = bfsFound;
                }
                visited.remove(toRemoveInd);
                toVisit.add(similar);
            }
            return bfsFoundMin;
        }
    }

    private HashMap<String, List<String>> cache = new HashMap<>();

    private List<String> findSimilar(final String current, final List<String> visited, final List<String> toVisit) {
        if (!cache.containsKey(current)) {
            final List<String> result = toVisit.stream().filter(item -> isSimilar(item, current)).collect(Collectors.toList());
            cache.put(current, result);
        }
        return cache.get(current);
    }

    private boolean isSimilar(final String toCompare, final String current) {
        int diff = 0;
        for (int i = 0; i < toCompare.length(); i++) {
            if (toCompare.charAt(i) != current.charAt(i)) {
                diff++;
            }
            if (diff > 1) {
                return false;
            }
        }
        return true;
    }

}
