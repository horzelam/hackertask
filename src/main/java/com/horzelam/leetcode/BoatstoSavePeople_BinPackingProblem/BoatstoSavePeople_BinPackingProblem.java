package com.horzelam.leetcode.BoatstoSavePeople_BinPackingProblem;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

public class BoatstoSavePeople_BinPackingProblem {
    public static void main(String[] args) {
        final BoatstoSavePeople_BinPackingProblem main = new BoatstoSavePeople_BinPackingProblem();

        int[] people;
        int limit;

        // case 1
        people = new int[]{1, 2};
        limit = 3;
        assertThat(main.numRescueBoats(people, limit)).isEqualTo(1);

        // case 2
        //        Input: people = [3,2,2,1], limit = 3
        //        Output: 3
        //        Explanation: 3 boats (1, 2), (2) and (3)
        people = new int[]{3, 2, 2, 1};
        limit = 3;
        assertThat(main.numRescueBoats(people, limit)).isEqualTo(3);

        // case 3
        //        Input: people = [3,5,3,4], limit = 5
        //        Output: 4
        //        Explanation: 4 boats (3), (3), (4), (5)
        people = new int[]{3, 5, 3, 4};
        limit = 5;
        assertThat(main.numRescueBoats(people, limit)).isEqualTo(4);

        // case 4
        people = new int[]{3, 2, 3, 2, 2};
        limit = 6;
        assertThat(main.numRescueBoats(people, limit)).isEqualTo(3);


        // case 5
        people = new int[]{5,1,4,2};
        limit = 6;
        assertThat(main.numRescueBoats(people, limit)).isEqualTo(2);


    }

    public int numRescueBoats(int[] people, int limit) {

        Arrays.sort(people);
        return firstFit(people, people.length, limit);

    }

    private int firstFit(final int[] people, final int length, final int limit) {

        int[] remaining_sizes = new int[length];
        int[] onboarded = new int[length];
        int remaining_sizes_amount = 0;

        for (int i = length - 1; i >= 0; i--) {

            int toFillLastFound = 0;
            for (; toFillLastFound < remaining_sizes_amount; toFillLastFound++) {
                if (remaining_sizes[toFillLastFound] >= people[i] && onboarded[toFillLastFound] < 2) {
                    remaining_sizes[toFillLastFound] -= people[i];
                    onboarded[toFillLastFound] += 1;
                    break;
                }
            }
            if (toFillLastFound == remaining_sizes_amount) {
                onboarded[remaining_sizes_amount] += 1;
                remaining_sizes[remaining_sizes_amount++] = limit - people[i];
            }

        }
        return remaining_sizes_amount;
    }

}
