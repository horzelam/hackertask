package com.horzelam.leetcode.BoatstoSavePeople_BinPackingProblem;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class BoatstoSavePeople_BinPackingProblem_Tree {
    public static void main(String[] args) throws IOException {
        final BoatstoSavePeople_BinPackingProblem_Tree main = new BoatstoSavePeople_BinPackingProblem_Tree();

        int[] people;
        int limit;
        //
        // case 1
        //        people = new int[]{1, 2};
        //        limit = 3;
        //        assertThat(main.numRescueBoats(people, limit)).isEqualTo(1);
        //
        //        // case 2
        //        //        Input: people = [3,2,2,1], limit = 3
        //        //        Output: 3
        //        //        Explanation: 3 boats (1, 2), (2) and (3)
        //        people = new int[]{3, 2, 2, 1};
        //        limit = 3;
        //        assertThat(main.numRescueBoats(people, limit)).isEqualTo(3);
        //
        //        // case 3
        //        //        Input: people = [3,5,3,4], limit = 5
        //        //        Output: 4
        //        //        Explanation: 4 boats (3), (3), (4), (5)
        //        people = new int[]{3, 5, 3, 4};
        //        limit = 5;
        //        assertThat(main.numRescueBoats(people, limit)).isEqualTo(4);
        //
        //        // case 4
        //        people = new int[]{3, 2, 3, 2, 2};
        //        limit = 6;
        //        assertThat(main.numRescueBoats(people, limit)).isEqualTo(3);

        // case
        people = new int[]{8, 2, 3, 6, 2, 6};
        limit = 8;
        assertThat(main.numRescueBoats(people, limit)).isEqualTo(4);

        List<String> filelines;
        // case 5
        filelines = Files.readAllLines(Paths.get(
                "/home/michal/workspace-priv/task/src/main/java/com/horzelam/leetcode/BoatstoSavePeople_BinPackingProblem/big_case.txt"));
        people = read(filelines);
        limit = 29999;
        assertThat(main.numRescueBoats(people, limit)).isEqualTo(25134);

        // case 6
        filelines = Files.readAllLines(Paths.get(
                "/home/michal/workspace-priv/task/src/main/java/com/horzelam/leetcode/BoatstoSavePeople_BinPackingProblem/big_case.2.txt"));
        people = read(filelines);
        limit = 100;
        assertThat(main.numRescueBoats(people, limit)).isEqualTo(25325);

        System.out.println("DONE");
        System.out.println("DONE");

    }

    private static int[] read(final List<String> filelines) {
        final List<Integer> result = new ArrayList<>();
        filelines.forEach(line -> {
            final String[] split = line.split(",");
            for (String item : split) {
                result.add(Integer.parseInt(item));
            }
        });
        System.out.println("Array has " + result.size() + " size . first elems:");
        result.stream().limit(10).forEach(item -> System.out.println(" - " + item));
        return result.stream().mapToInt(i -> i).toArray();
    }

    public int numRescueBoats(int[] people, int limit) {

        Arrays.sort(people);
        return firstFit(people, people.length, limit);

    }

    class Tree {
        node mainRoot;

        // An AVL tree node
        class node {
            int key;
            node left;
            node right;
            int height;
            int count;
        }

        // A utility function to get height of the tree
        int height(node N) {
            if (N == null) {
                return 0;
            }
            return N.height;
        }

        // A utility function to get maximum of two integers
        int max(int a, int b) {
            return (a > b) ? a : b;
        }

        /* Helper function that allocates a new node with the given key and
        null left and right pointers. */
        node newNode(int key) {
            node node = new node();
            node.key = key;
            node.left = null;
            node.right = null;
            node.height = 1; // new node is initially added at leaf
            node.count = 1;
            return (node);
        }

        // A utility function to right rotate subtree rooted with y
        // See the diagram given above.
        node rightRotate(node y) {
            node x = y.left;
            node T2 = x.right;

            // Perform rotation
            x.right = y;
            y.left = T2;

            // Update heights
            y.height = max(height(y.left), height(y.right)) + 1;
            x.height = max(height(x.left), height(x.right)) + 1;

            // Return new root
            return x;
        }

        // A utility function to left rotate subtree rooted with x
        // See the diagram given above.
        node leftRotate(node x) {
            node y = x.right;
            node T2 = y.left;

            // Perform rotation
            y.left = x;
            x.right = T2;

            // Update heights
            x.height = max(height(x.left), height(x.right)) + 1;
            y.height = max(height(y.left), height(y.right)) + 1;

            // Return new root
            return y;
        }

        // Get Balance factor of node N
        int getBalance(node N) {
            if (N == null) {
                return 0;
            }
            return height(N.left) - height(N.right);
        }

        node insert(node node, int key) {
            /*1.  Perform the normal BST rotation */
            if (node == null) {
                return (newNode(key));
            }

            // If key already exists in BST, increment count and return
            if (key == node.key) {
                (node.count)++;
                return node;
            }

            /* Otherwise, recur down the tree */
            if (key < node.key) {
                node.left = insert(node.left, key);
            } else {
                node.right = insert(node.right, key);
            }

            /* 2. Update height of this ancestor node */
            node.height = max(height(node.left), height(node.right)) + 1;

        /* 3. Get the balance factor of this ancestor node to check whether
       this node became unbalanced */
            int balance = getBalance(node);

            // If this node becomes unbalanced, then there are 4 cases

            // Left Left Case
            if (balance > 1 && key < node.left.key) {
                return rightRotate(node);
            }

            // Right Right Case
            if (balance < -1 && key > node.right.key) {
                return leftRotate(node);
            }

            // Left Right Case
            if (balance > 1 && key > node.left.key) {
                node.left = leftRotate(node.left);
                return rightRotate(node);
            }

            // Right Left Case
            if (balance < -1 && key < node.right.key) {
                node.right = rightRotate(node.right);
                return leftRotate(node);
            }

            /* return the (unchanged) node pointer */
            return node;
        }

        /* Given a non-empty binary search tree, return the node with minimum
       key value found in that tree. Note that the entire tree does not
       need to be searched. */
        node minValueNode(node node) {
            node current = node;

            /* loop down to find the leftmost leaf */
            while (current.left != null) {
                current = current.left;
            }

            return current;
        }

        node deleteNode(node root, int key) {
            // STEP 1: PERFORM STANDARD BST DELETE

            if (root == null) {
                return root;
            }

            // If the key to be deleted is smaller than the root's key,
            // then it lies in left subtree
            if (key < root.key) {
                root.left = deleteNode(root.left, key);
            }

            // If the key to be deleted is greater than the root's key,
            // then it lies in right subtree
            else if (key > root.key) {
                root.right = deleteNode(root.right, key);
            }

            // if key is same as root's key, then This is the node
            // to be deleted
            else {
                // If key is present more than once, simply decrement
                // count and return
                if (root.count > 1) {
                    (root.count)--;
                    return root; // BUG - was null returned
                }
                // ElSE, delete the node

                // node with only one child or no child
                if ((root.left == null) || (root.right == null)) {
                    node temp = root.left != null ? root.left : root.right;

                    // No child case
                    if (temp == null) {
                        temp = root;
                        root = null;
                    } else // One child case
                    {
                        root = temp; // Copy the contents of the non-empty child
                    }
                } else {
                    // node with two children: Get the inorder successor (smallest
                    // in the right subtree)
                    node temp = minValueNode(root.right);

                    // Copy the inorder successor's data to this node and update the count
                    root.key = temp.key;
                    root.count = temp.count;
                    temp.count = 1;

                    // Delete the inorder successor
                    root.right = deleteNode(root.right, temp.key);
                }
            }

            // If the tree had only one node then return
            if (root == null) {
                return root;
            }

            // STEP 2: UPDATE HEIGHT OF THE CURRENT NODE
            root.height = max(height(root.left), height(root.right)) + 1;

            // STEP 3: GET THE BALANCE FACTOR OF THIS NODE (to check whether
            // this node became unbalanced)
            int balance = getBalance(root);

            // If this node becomes unbalanced, then there are 4 cases

            // Left Left Case
            if (balance > 1 && getBalance(root.left) >= 0) {
                return rightRotate(root);
            }

            // Left Right Case
            if (balance > 1 && getBalance(root.left) < 0) {
                root.left = leftRotate(root.left);
                return rightRotate(root);
            }

            // Right Right Case
            if (balance < -1 && getBalance(root.right) <= 0) {
                return leftRotate(root);
            }

            // Right Left Case
            if (balance < -1 && getBalance(root.right) > 0) {
                root.right = rightRotate(root.right);
                return leftRotate(root);
            }

            return root;
        }

        // A utility function to print preorder traversal of the tree.
        // The function also prints height of every node
        void preOrder(node root) {
            if (root != null) {
                System.out.printf("%d(%d) ", root.key, root.count);
                preOrder(root.left);
                preOrder(root.right);
            }
        }

        boolean findSmallestMatchingAndRemoveIt(node node, final int weight) {
            if (node == null) {
                return false;
            }
            if (node.key >= weight) {
                if (findSmallestMatchingAndRemoveIt(node.left, weight)) {
                    return true;
                } else {
                    // then current is matching
                    mainRoot = deleteNode(mainRoot, node.key);
                    return true;
                }
            } else {
                return findSmallestMatchingAndRemoveIt(node.right, weight);
            }
        }
    }

    private int firstFit(final int[] people, final int length, final int limit) {
        Tree tree = new Tree();

        int boats = 0;
        for (int i = length - 1; i >= 0; i--) {

            // find if fits and delete and rebalance
            if (people[i] == limit) {
                boats++;
            } else if (tree.findSmallestMatchingAndRemoveIt(tree.mainRoot, people[i])) {
                // removed
            } else {
                // if not - just insert
                tree.mainRoot = tree.insert(tree.mainRoot, limit - people[i]);
                //tree.preOrder(tree.mainRoot);
                boats++;
            }
        }
        return boats;
    }

}
