package com.horzelam.leetcode.BoatstoSavePeople_BinPackingProblem;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class BoatstoSavePeople_BinPackingProblem_RuntimeRemoval {
    public static void main(String[] args) throws IOException {
        final BoatstoSavePeople_BinPackingProblem_RuntimeRemoval main = new BoatstoSavePeople_BinPackingProblem_RuntimeRemoval();

        int[] people;
        int limit;

        // case 1
        people = new int[]{1, 2};
        limit = 3;
        assertThat(main.numRescueBoats(people, limit)).isEqualTo(1);

        // case 2
        //        Input: people = [3,2,2,1], limit = 3
        //        Output: 3
        //        Explanation: 3 boats (1, 2), (2) and (3)
        people = new int[]{3, 2, 2, 1};
        limit = 3;
        assertThat(main.numRescueBoats(people, limit)).isEqualTo(3);

        // case 3
        //        Input: people = [3,5,3,4], limit = 5
        //        Output: 4
        //        Explanation: 4 boats (3), (3), (4), (5)
        people = new int[]{3, 5, 3, 4};
        limit = 5;
        assertThat(main.numRescueBoats(people, limit)).isEqualTo(4);

        // case 4
        people = new int[]{3, 2, 3, 2, 2};
        limit = 6;
        assertThat(main.numRescueBoats(people, limit)).isEqualTo(3);


        // case 5
        people = new int[]{5,1,4,2};
        limit = 6;
        assertThat(main.numRescueBoats(people, limit)).isEqualTo(2);

        List<String> filelines;
//        // case 5
//        filelines = Files.readAllLines(Paths.get(
//                "/home/michal/workspace-priv/task/src/main/java/com/horzelam/leetcode/BoatstoSavePeople_BinPackingProblem/big_case.txt"));
//        people = read(filelines);
//        limit = 29999;
//        assertThat(main.numRescueBoats(people, limit)).isEqualTo(25134);


        // case 6
         filelines = Files.readAllLines(Paths.get(
                "/home/michal/workspace-priv/task/src/main/java/com/horzelam/leetcode/BoatstoSavePeople_BinPackingProblem/big_case.2.txt"));
        people = read(filelines);
        limit = 100;
        assertThat(main.numRescueBoats(people, limit)).isEqualTo(25325);


        System.out.println("DONE");

    }

    private static int[] read(final List<String> filelines) {
        final List<Integer> result = new ArrayList<>();
        filelines.forEach(line -> {
            final String[] split = line.split(",");
            for (String item:split){
                result.add(Integer.parseInt(item));
            }
        });
        System.out.println("Array has " + result.size() + " size . first elems:");
        result.stream().limit(10).forEach(item -> System.out.println(" - " + item));
        return result.stream().mapToInt(i->i).toArray();
    }

    public int numRescueBoats(int[] people, int limit) {

        long start = System.currentTimeMillis();
        Arrays.sort(people);
        System.out.println("-sort time: " + (System.currentTimeMillis() - start) + " ms");
        start = System.currentTimeMillis();
        final int res = firstFit(people, people.length, limit);
        System.out.println("-fit time: " + (System.currentTimeMillis() - start) + " ms");
        return res;

    }

    class Node{
        int remaining_sizes;
        int onboarded;
        Node next;

        public Node(final int remaining_sizes, final int onboarded) {
            this.remaining_sizes = remaining_sizes;
            this.onboarded = onboarded;
        }
    }

    private int firstFit(final int[] people, final int length, final int limit) {

        Node head = null;

        int boatsCreated = 0;

        for (int i = length - 1; i >= 0; i--) {
            Node prev = null;
            Node curr = head;
            while (curr != null) {
                if (curr.remaining_sizes >= people[i] && curr.onboarded < 2) {
                    // Adding 2nd person and removing Node !!
                    curr.remaining_sizes -= people[i];
                    curr.onboarded += 1;
                    if(prev != null) {
                        prev.next = curr.next;
                    } else {
                        head = curr.next;
                    }
                    break;
                }
                prev = curr;
                curr = curr.next;
            }
            if (curr == null) {
                if(prev ==null){
                    head = new Node(limit - people[i], 1);
                }else {
                    prev.next = new Node(limit - people[i], 1);
                }
                boatsCreated++;
            }

        }
        return boatsCreated;
    }

}
