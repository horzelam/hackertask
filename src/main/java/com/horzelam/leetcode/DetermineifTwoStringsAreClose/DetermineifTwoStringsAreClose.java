package com.horzelam.leetcode.DetermineifTwoStringsAreClose;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class DetermineifTwoStringsAreClose {
    public static void main(String[] args) {
        final DetermineifTwoStringsAreClose main = new DetermineifTwoStringsAreClose();
        String w1, w2;

        w1 = "abc";
        w2 = "bca";
        assertThat(main.closeStrings(w1, w2)).isTrue();

        w1 = "cabbba";
        w2 = "abbccc";
        assertThat(main.closeStrings(w1, w2)).isTrue();

        w1 = "cabbba";
        w2 = "aabbss";
        assertThat(main.closeStrings(w1, w2)).isFalse();

        w1 = "uau";
        w2 = "ssx";
        assertThat(main.closeStrings(w1, w2)).isFalse();
    }

    public boolean closeStrings(String word1, String word2) {
        if (word1.length() != word2.length()) {
            return false;
        }

        int[] map1 = new int[26];
        int[] map2 = new int[26];
        for (int i = 0; i < word1.length(); i++) {
            map1[word1.charAt(i) - 'a']++;
            map2[word2.charAt(i) - 'a']++;
        }

        Map<Integer,Integer> f1 = new HashMap<>();
        Map<Integer,Integer> f2 = new HashMap<>();
        for (int i = 0; i < 26; i++) {
            if (map1[i] != 0 && map2[i] == 0 || map1[i] == 0 && map2[i] != 0) {
                return false;
            }
            if (map1[i] != 0 && map2[i] != 0) {

                int val = f1.getOrDefault(map1[i],0) + 1;
                f1.put(map1[i], val);
                val = f2.getOrDefault(map2[i],0) + 1;
                f2.put(map2[i], val);
            }
        }

//        System.out.println("f1: " + (f1));
//        System.out.println("f2: " + (f2));
        return f1.equals(f2);

    }
}
