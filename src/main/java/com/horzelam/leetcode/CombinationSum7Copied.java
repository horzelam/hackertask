package com.horzelam.leetcode;

import java.util.ArrayList;
import java.util.List;

public class CombinationSum7Copied {

    public static void main(String[] args) {
//        /// case 1
        int[] candidates = new int[]{2, 7, 6, 3, 5, 1};
        int target = 9;
        Long start = System.currentTimeMillis();
        System.out.println("    Curr: " + combinationSum(candidates, target) + " -- " + (System.currentTimeMillis() - start) + " ms");
        System.out.println(" vs Expt: [[1, 1, 1, 1, 1, 1, 1, 1, 1], [1, 1, 1, 1, 1, 1, 1, 2], [1, 1, 1, 1, 1, 1, 3], " +
                "[1, 1, 1, 1, 1, 2, 2], [1, 1, 1, 1, 2, 3], [1, 1, 1, 1, 5], [1, 1, 1, 2, 2, 2], [1, 1, 1, 3, 3], [1," +
                " 1, 1, 6], [1, 1, 2, 2, 3], [1, 1, 2, 5], [1, 1, 7], [1, 2, 2, 2, 2], [1, 2, 3, 3], [1, 2, 6], [1, " +
                "3, 5], [2, 2, 2, 3], [2, 2, 5], [2, 7], [3, 3, 3], [3, 6]]");
    }

    static List<List<Integer>> result = new ArrayList<>();

    public static List<List<Integer>> combinationSum(int[] candidates, int target) {
        if (candidates == null) return result;
        dfs(candidates, 0, 0, new ArrayList<Integer>(), target);
        return result;
    }

    private static void dfs(int[] array, int index, int sum, List<Integer> combSum, int target) {
        System.out.println("--check" + combSum);
        if (sum == target) {
            result.add(new ArrayList<>(combSum));
            return;
        }
        if (sum > target) return;
        while (index < array.length) {
            sum += array[index];
            // we always create copies of our current path
            List<Integer> comb = new ArrayList<>(combSum);
            comb.add(array[index]);
            dfs(array, index, sum, comb, target);
            //when we return, we have to remove the last value we added
            int indexToRemove = comb.size() - 1;
            // we also have to remove value from sum
            sum -= comb.get(indexToRemove);
            index++;
            comb.remove(indexToRemove);
        }

    }
}