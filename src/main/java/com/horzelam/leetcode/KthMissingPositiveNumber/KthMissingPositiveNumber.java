package com.horzelam.leetcode.KthMissingPositiveNumber;

import static org.assertj.core.api.Java6Assertions.assertThat;

public class KthMissingPositiveNumber {
    public static void main(String[] args) {

        final KthMissingPositiveNumber main = new KthMissingPositiveNumber();

        int[] arr = new int[]{2, 3, 4, 7, 11};
        int k = 5;
        int result = main.findKthPositive(arr, k);
        assertThat(result).isEqualTo(9);

        arr = new int[]{1, 2, 3, 4};
        k = 2;
        result = main.findKthPositive(arr, k);
        assertThat(result).isEqualTo(6);
    }

    public int findKthPositive(int[] arr, int k) {

        int expectedVal = 0;
        int missingCount = 0;
        int i = 0;
        while (missingCount < k) {
            ++expectedVal;
            if (i==arr.length || arr[i] != expectedVal) {
                missingCount++;
            } else {
                i++;
            }
        }
        if (missingCount == k) {
            return expectedVal;
        } else {
            return -1;
        }
    }
}
