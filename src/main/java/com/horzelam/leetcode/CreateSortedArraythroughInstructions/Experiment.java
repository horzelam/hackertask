package com.horzelam.leetcode.CreateSortedArraythroughInstructions;

import static org.assertj.core.api.Assertions.assertThat;

public class Experiment {
    public static void main(String[] args) {

        final Experiment m = new Experiment();

        int[] instructions;
        // case 1
        instructions = new int[]{1, 5, 6, 2};
        assertThat(m.createSortedArray(instructions)).isEqualTo(1);

        // case 2
        instructions = new int[]{1, 2, 3, 6, 5, 4};
        assertThat(m.createSortedArray(instructions)).isEqualTo(3);
        //
        // case 4
        instructions = new int[]{1, 3, 3, 3, 2, 4, 2, 1, 2};
        assertThat(m.createSortedArray(instructions)).isEqualTo(4);

        // case 5
        instructions = new int[]{4,14,10,2,5,3,8,19,7,20,12,1,9,15,13,11,18,6,16,17};
        assertThat(m.createSortedArray(instructions)).isEqualTo(43);
    }

    int high = 100000;
    int[] arr = new int[high + 1];

    void add(int x) {
        while (x <= high) {
            arr[x]++;
            x += x & (-x);
        }
    }

    int q(int x) {
        if (x == 0) {
            return x;
        }
        int ret = 0;
        // ?
        while (x>0) {
            ret += arr[x];
            x -= x & (-x);
        }
        return ret;
    }

    int createSortedArray(int[] instructions) {

        int N = instructions.length;
        int sum = 0;
        int mod = 1000000000 + 7;
        for (int i = 0; i < N; i++) {
            add(instructions[i]);
            int left = q(instructions[i] - 1);
            int right = i + 1 - q(instructions[i]);
            sum = (sum + Math.min(left, right) % mod) % mod;
        }
        return sum;
    }

}
