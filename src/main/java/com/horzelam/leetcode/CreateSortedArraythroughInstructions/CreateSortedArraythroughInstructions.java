package com.horzelam.leetcode.CreateSortedArraythroughInstructions;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

public class CreateSortedArraythroughInstructions {
    public static void main(String[] args) {

        final CreateSortedArraythroughInstructions m = new CreateSortedArraythroughInstructions();
        int[] instructions;
        // case 1
        instructions = new int[]{1, 5, 6, 2};
        assertThat(m.createSortedArray(instructions)).isEqualTo(1);

        // case 2
        instructions = new int[]{1, 2, 3, 6, 5, 4};
        assertThat(m.createSortedArray(instructions)).isEqualTo(3);

        // case 4
        //instructions = new int[]{1,3,3,3,2,4,2,1,2};
        //assertThat(m.createSortedArray(instructions)).isEqualTo(1);

    }

    public int createSortedArray(int[] instructions) {

        if (instructions.length < 2) {
            return 0;
        }
        final int[] temp = new int[instructions.length];
        final int val = mergeSort(instructions, 0, instructions.length - 1, temp);
        System.out.println(Arrays.toString(temp));
        return val;
    }

    private int mergeSort(final int[] instructions, int start, int end, final int[] temp) {
        if (start >= end) {
            return 0;
        } else {
            int newMid = start + (end - start) / 2;
            int minInvCount1 = mergeSort(instructions, start, newMid, temp);
            int minInvCount2 = mergeSort(instructions, newMid + 1, end, temp);
            int minInvCount3 = merge(instructions, start, newMid, end, temp);
            return minInvCount1 + minInvCount2 + minInvCount3;
        }
    }

    private int merge(final int[] arr, final int start, final int mid, final int end, final int[] temp) {
        int i = start;
        int j = mid + 1;
        int k = start;
        int invCount = 0;
        while (k <= end) {
            int cost = 0;
            if (i > mid) {
                temp[k] = arr[j];
                j++;
            } else if (j > end) {
                temp[k] = arr[i];
                i++;
                //invCount += Math.min(mid + 1 - i, k - start);
            } else if (arr[i] < arr[j]) {
                temp[k] = arr[i];
                i++;
            } else {
                temp[k] = arr[j];
                cost = mid + 1 - i;
                System.out.println("For elem: "+arr[j]+" cost: "+cost+"  when merging " + Arrays.toString(arr) + " start: " + start + " mid: " + mid + " end:" + end);
                //System.out.println("For : "+arr[j]+" inc by cost: min of(" + (mid + 1 + -i)+ "," + (k - start)+") +  when merging " + Arrays.toString(arr) + " start: " + start + " mid: " + mid + " end:" + end);
                //cost += Math.min(mid + 1 - i, k - start);
                j++;
            }
            invCount += cost;
            k++;
        }

        // update original array
        for (int p = start; p <= end; p++) {
            arr[p] = temp[p];
        }
        return invCount;
    }
}
