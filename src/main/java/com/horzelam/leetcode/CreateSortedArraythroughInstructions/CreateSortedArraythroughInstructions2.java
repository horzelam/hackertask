package com.horzelam.leetcode.CreateSortedArraythroughInstructions;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

public class CreateSortedArraythroughInstructions2 {
    public static void main(String[] args) {

        final CreateSortedArraythroughInstructions2 m = new CreateSortedArraythroughInstructions2();
        int[] instructions;
        // case 1
        instructions = new int[]{1, 5, 6, 2};
        assertThat(m.createSortedArray(instructions)).isEqualTo(1);

        // case 2
        instructions = new int[]{1, 2, 3, 6, 5, 4};
        assertThat(m.createSortedArray(instructions)).isEqualTo(3);
        //
        // case 4
        instructions = new int[]{1, 3, 3, 3, 2, 4, 2, 1, 2};
        assertThat(m.createSortedArray(instructions)).isEqualTo(4);

        // case 5
        instructions = new int[]{4,14,10,2,5,3,8,19,7,20,12,1,9,15,13,11,18,6,16,17};
        assertThat(m.createSortedArray(instructions)).isEqualTo(43);

        //case 6
        instructions = new int[]{125,279,319,225,112,124,248,46,305,100,26,261,237,181,138,162,160,182,121,228,335,154,107,165,95,102,86,16,245,137,285,268,141,272,247,166,63,284,263,131,5,310,75,118,311,155,281,47,35,92,43,227,134,70,276,179,170,316,7,53,280,12,89,90,40,13,136,84,4,80,295,322,10,38,171,126,215,232,264,259,145,41,120,262,132,173,159,202,282,190,197,61,189,223,211,81,73,233,114,116,85,221,278,71,32,151,74,139,48,269,297,207,93,39,50,20,29,239,257,153,28,133,217,69,265,44,219,201,231,254,60,267,260,266,327,337,306,301,332,308,238,323,22,57,175,128,209,33,336,143,243,253,294,76,235,122,64,108,192,103,230,291,300,241,174,256,178,298,274,58,140,45,97,19,188,146,49,110,208,317,30,275,129,216,11,117,185,191,122,184,194,158,113,204,270,130,111,220,18,326,320,65,286,101,288,203,78,200,6,180,293,149,150,318,333,187,55,296,62,9,224,99,168,56,222,68,83,299,98,314,77,88,24,236,242,79,148,321,164,307,330,135,246,277,91,304,251,212,240,334,258,51,325,34,290,59,27,152,8,82,289,96,104,196,119,163,3,21,303,183,302,106,169,309,25,1,287,36,66,313,105,271,186,17,147,127,214,213,205,331,206,161,176,273,315,172,144,252,312,37,42,226,142,52,115,123,54,15,249,156,87,229,283,109,193,72,167,195,23,218,94,292,329,210,157,338,328,2,198,244,14,250,255,177,31,199,234,324,67};
        assertThat(m.createSortedArray(instructions)).isEqualTo(13748);
    }

    class Elem {
        int amountSmaller;
        int amountSwaps;
        int val;
        final int orgInd;

        public Elem(final int amountSmaller, final int amountSwaps, final int val, final int orgInd) {
            this.amountSmaller = amountSmaller;
            this.amountSwaps = amountSwaps;
            this.val = val;
            this.orgInd = orgInd;
        }
//
//        @Override
//        public String toString() {
//            return new StringJoiner(", ", "[", "]").add("val=" + val).toString();
//        }
//
//        public String costToString() {
//            return "(" + amountSmaller + "," + amountSwaps + ")";
//        }
//
//        public String allToString() {
//            return val + "(" + amountSmaller + "," + amountSwaps + ") @ " + orgInd;
//        }
    }

    public int createSortedArray(int[] instructions) {

        if (instructions.length < 2) {
            return 0;
        }
        final Elem[] arr = new Elem[instructions.length];
        final Elem[] temp = new Elem[instructions.length];
        for (int i = 0; i < instructions.length; i++) {
            arr[i] = new Elem(0, 0, instructions[i], i);
        }
        mergeSort(arr, 0, instructions.length - 1, temp);

        //Arrays.stream(temp).forEach(elem -> System.out.println(elem.allToString()));

        return Arrays.stream(temp).mapToInt(el -> Math.min(el.amountSmaller, el.amountSwaps)).sum() %( 1000000007);
    }

    private void mergeSort(final Elem[] arr, int start, int end, final Elem[] temp) {
        if (start >= end) {
            return;
        } else {
            int newMid = start + (end - start) / 2;
            mergeSort(arr, start, newMid, temp);
            mergeSort(arr, newMid + 1, end, temp);
            merge(arr, start, newMid, end, temp);
        }
    }

    private void merge(final Elem[] arr, final int start, final int mid, final int end, final Elem[] temp) {
        int i = start;
        int j = mid + 1;
        int k = start;
        int amountSmOnLeft = 0;
        while (k <= end) {
            int cost = 0;
            if (i > mid) {
                getSmaller(arr, temp, j, k, amountSmOnLeft, start);
                //                System.out.println("Elem: " + arr[j].allToString() + " got amountSmaller + x ->  " + "  when merging " +
//                        MergToStr(arr, start, mid, end));
                temp[k] = arr[j];
                j++;
            } else if (j > end) {
                temp[k] = arr[i];
                i++;
            } else if (arr[i].val <= arr[j].val) {
                temp[k] = arr[i];
                amountSmOnLeft++;
                i++;
            } else {
                cost = mid + 1 - i;
                arr[j].amountSwaps += cost;
                getSmaller(arr, temp, j, k, amountSmOnLeft, start);
                temp[k] = arr[j];
//                System.out.println("Elem: " + arr[j].allToString() + " got amountSwaps + " + cost + " -> " + "  when merging " +
//                        MergToStr(arr, start, mid, end));
                j++;
            }
            k++;
        }

        for (int p = start; p <= end; p++) {
            arr[p] = temp[p];
        }
    }

    private void getSmaller(final Elem[] arr, final Elem[] temp, final int j, final int k, final int amountSmOnLeft, final int start) {
        if (k > start && temp[k - 1].val == arr[j].val) {
            arr[j].amountSmaller += temp[k - 1].amountSmaller;
        } else {
            arr[j].amountSmaller += amountSmOnLeft;
        }
    }

    private String MergToStr(final Elem[] arr, final int start, final int mid, final int end) {
        return " <start: " + start + " mid: " + mid + " end:" + end + "> " + Arrays.toString(Arrays.copyOfRange(arr, start, end + 1));
    }
}
