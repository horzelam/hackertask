

object Solution {

  def numberOfWays(Min: Int, X: Int, N: Int, list:List[Int]): Int = {
    // Compute the answer in this function over here
    // It is fine to define new functions as and where required

    // println("Iter - Min: " + Min + " , X: " + X + " -- "+ list)
    var maxChecked = math.ceil(math.sqrt(X)).toInt; //its too much , but it's just estimation

    (Min to maxChecked).map(candidate => {
      //println(((1 to Min).map(a=>"-").foldLeft("")(_+_)) + " candidate : " + candidate)

      var left = (X - math.pow(candidate, N)).toInt;

      if (left == 0) {
        //println(((1 to Min).map(a=>"-").foldLeft("")(_+_))  + " FOUND solution: " + (list ++ List(candidate)))
        1;
      }
      else if (left < math.pow(Min + 1, N).toInt) {
        //println("---- not found for last candidate : " + candidate + " ; left: " + left);
        0;
      } else {
        numberOfWays(candidate + 1, left, N, list ++ List(candidate))
      }
    }).foldLeft(0)(_ + _);

  }

  def numberOfWays(X: Int, N: Int): Int = {
    numberOfWays(1, X, N, List());
  }

  def main(args: Array[String]) {
    //println(numberOfWays(readInt(),readInt()))
    //println("10,2  : " + numberOfWays(10, 2));

    println("100,2 : " + numberOfWays(100, 2));
    //println("100,3 : " + numberOfWays(100, 3));
  }
}

