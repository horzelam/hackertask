package com.horzelam.convex_hull

import java.text.DecimalFormat

/**
  * Created by osboxes on 11/01/17.
  */
object Solution {

  def runReal() = {
    val n:Int = readLine().trim.toInt

    // reading lines and creating the structure:
    val struct = new Structure();
    (1 to n).foreach(line => {
      val line = readLine().trim.split(" ")
      struct.addPoint(Point(line(0).toDouble, line(1).toDouble))
    })

    // calculating and printing result with max 2 digits
    println(
      new java.text.DecimalFormat("#.##").format(
        BigDecimal(struct.calculatePerimeter()).setScale(1, BigDecimal.RoundingMode.HALF_UP))
    )
  }

  def main(args: Array[String]): Unit = {

    //    // my unit tests
    //    tests()
    //
    //    // real examples
    //    complexStructureTest()
  bigStructureTest()

    //runReal()
  }

  def tests() = {
    testPerpendicularLine()
    testPart()
    testNormalizedVec()
    testCastVec()
    testSimpleTriangle()
    testSimpleSquare()
  }

  def testCastVec() = {
    val part = Part(Point(1.0, 1.0), Point(5.0, 3.0), Point(2, 5))
    val castVec = part.normalizedVectorToPoint(Point(3, 2))
    debug("castVec = " + castVec)
    assert(castVec == zeroVector, "Cast vector of point going through the line of Part should be zero")
  }

  def testNormalizedVec() = {

    val n4 = Part(Point(2.0, 5.0), Point(3.0, 3.0), Point(1.0, 1.0)).normalVector
    assert((n4.end.x + 1) <= accuracy, "Simple Normal Vector to Part calc failed")
    assert((n4.end.y + 0.5) <= accuracy, "Simple Normal Vector to Part calc failed")

    val n1 = Part(Point(1.0, 1.0), Point(2.0, 5.0), Point(3.0, 3.0)).normalVector
    assert(n1.end.x == 1, "Simple Normal Vector to Part calc failed")
    assert((n1.end.y + 0.25) <= accuracy, "Simple Normal Vector to Part calc failed")

    val n2 = Part(Point(0, 0), Point(0, 1), Point(1, 1)).normalVector
    assert(n2.end.x == 1, "Simple Normal Vector to Part calc failed")
    assert((n2.end.y - 0) <= accuracy, "Simple Normal Vector to Part calc failed")

    val n3 = Part(Point(0, 0), Point(1, 0), Point(1, 1)).normalVector
    assert(n3.end.x == 0, "Simple Normal Vector to Part calc failed")
    assert((n3.end.y - 1) <= accuracy, "Simple Normal Vector to Part calc failed")


  }

  def testPerpendicularLine() = {
    // original : y = 0x + 1 ==> -0x + 1y - 1 = 0
    val line = Line(0, 1, -1)

    val perpendicularLine = line.getPerpendicular(Point(3, 3))

    // expected : x = 0y + 3 ==> 1x - 0y - 3 = 0 or -1x - 0y + 3 = 0
    assert(perpendicularLine.A == -1)
    assert(perpendicularLine.B == 0)
    assert(perpendicularLine.C == 3)
  }

  def testPart(): Unit = {
    val partLine1 = Part(Point(0, 0), Point(1, 0), Point(1, 1)).line
    assert(partLine1.A == 0)
    assert(partLine1.B == 1)
    assert(partLine1.C == 0)


    val partLine2 = Part(Point(0, 0), Point(1, 1), Point(1, 0)).line
    assert(partLine2.A == -1)
    assert(partLine2.B == 1)
    assert(partLine2.C == 0)


    val partLine3 = Part(Point(0, 0), Point(0, 1), Point(1, 1)).line
    assert(partLine3.A == -1)
    assert(partLine3.B == 0)
    assert(partLine3.C == 0)
  }

  def testSimpleTriangle() = {
    var structure = Structure();
    structure.addPoint(Point(1, 1))
    structure.addPoint(Point(3, 1))
    structure.addPoint(Point(2, 3))
    assert((structure.calculatePerimeter() - 6.472135955) <= finalAccuracy, "Simple triangle perimiter calc failed")
  }

  def testSimpleSquare() = {
    var structure = Structure();
    structure.addPoint(Point(1, 1))
    structure.addPoint(Point(3, 1))
    structure.addPoint(Point(3, 3))
    structure.addPoint(Point(1, 3))
    assert((structure.calculatePerimeter() - 8) <= finalAccuracy, "Simple square perimiter calc failed")
  }

  def bigStructureTest() = {
    var structure = Structure();

    structure.addPoint(Point(1, 1))
    structure.addPoint(Point(2, 5))
    structure.addPoint(Point(3, 3))
    structure.addPoint(Point(5, 3))
    structure.addPoint(Point(3, 2))
    structure.addPoint(Point(2, 2))
    structure.addPoint(Point(0,0))

    (1 to 500).foreach( x => structure.addPoint(Point(x,x)))

    structure.addPoint(Point(1000,0))
    structure.addPoint(Point(0,1000))
    val perimeter = structure.calculatePerimeter();
    debug(perimeter)
    assert( (perimeter - (math.sqrt(2)*1000 + 2*1000)).abs < finalAccuracy )

  }

  def complexStructureTest() = {
    var structure = Structure();
    // test case:
    var numOfLines = 6

    structure.addPoint(Point(1, 1))
    structure.addPoint(Point(2, 5))
    structure.addPoint(Point(3, 3))
    structure.addPoint(Point(5, 3))
    structure.addPoint(Point(3, 2))
    structure.addPoint(Point(2, 2))

    //debug(structure.calculatePerimeter())
    assert((structure.calculatePerimeter() - 12.2).abs < finalAccuracy)
  }

  def distance(pointA: Point, pointB: Point): Double =
    math.sqrt(math.pow((pointB.x - pointA.x).abs, 2) + math.pow((pointB.y - pointA.y).abs, 2))

  def createNormalizedVector(pointStart: Point, pointEnd: Point): Vec = {
    val xDelta = pointEnd.x - pointStart.x
    val yDelta = pointEnd.y - pointStart.y
    val scale = math.max(xDelta.abs, yDelta.abs)
    val x = xDelta / scale
    val y = yDelta / scale
    Vec(Point(0, 0), Point(x, y))
  }

  def areSimilar(castVector: Vec, normalVector: Vec): Boolean =
    (castVector.end.x - normalVector.end.x).abs <= accuracy &&
      (castVector.end.y - normalVector.end.y).abs <= accuracy

  case class Structure() {

    var points: List[Point] = List()
    var partList = List[Part]()
    var initialized = false

    def calculatePerimeter() = this.partList.map(_.size).sum

    def addPoint(point: Point): List[Part] = {
      if (!initialized) {
        points = points ::: List(point)
        if (points.size == 3) {
          add(Part(points(0), points(1), points(2)))
          add(Part(points(1), points(2), points(0)))
          add(Part(points(2), points(0), points(1)))
          initialized = true;
        }
      }
      else {
        // no more points adding - just lines

        debug("---- NEW POINT adding: " + point + "  to structure of " + this.partList.size)
        var listToRemove = getLinesToRemove(point)

        if (!listToRemove.isEmpty) {
          debug("--extending structure by:" + point)
          this.change(point, listToRemove)
          this.partList.foreach(part => {
            debug("--structure part: " + part)
          })
          debug("--extending finished----------")
        } else {
          debug("--ignoring point: " + point)
        }
      }

      var result = List[Part]()
      return result;
    }

    def getLinesToRemove(point: Point): List[Part] =
      partList.filter(part => isPartToRemove(part, point))


    def isPartToRemove(part: Part, point: Point): Boolean = {

      //val castPoint = calculatePointCast(part: Part, point: Point)
      // if (castPoint == point) {
      val castVector = part.normalizedVectorToPoint(point: Point)
      if (castVector == zeroVector) {
        // if point is already on the line = the same as casted point
        if (isLinePointWithinThePart(part, point)) {
          // and its outside the part -> then part must be removed (will be replaced by new longer one)
          debug("--Point outside part -> part to be removed: " + part)
          return true
        } else {
          // otherwise - just ignore it
          debug("--Ignoring point inside existing line: " + point)
          return false
        }
      } else {
        // get castVector as castPoint->point
        //val castVector = createNormalizedVector(castPoint, point)
        debug("--comparing cast vector: " + castVector + " - with part vector: " + part.normalVector)
        if (!areSimilar(castVector, part.normalVector)) {
          // if the Normal vector of part is different than castVector => line should be removed
          debug("--Point has normal vector against part different than part's own -> part to be removed: " + part)
          return true
        } else {
          debug("--Part " + part + " stays for point : " + point)
          return false
        }
      }
    }

    // checks if point on line of given part is actually included in the part
    def isLinePointWithinThePart(part: Part, point: Point): Boolean =
      !((math.min(part.p1.x, part.p2.x) <= point.x) && (point.x <= math.max(part.p1.x, part.p2.x))
        && (math.min(part.p1.y, part.p2.y) <= point.y) && (point.y <= math.max(part.p1.y, part.p2.y)))


    def change(point: Point, listToRemove: List[Part]) = {
      // remove the old parts
      this.partList = this.partList.filter(candidate => !listToRemove.contains(candidate))

      // get 2 points which doesn't repeat in parts
      val takenPoints = (if(this.partList.size <= listToRemove.size) partList      else       listToRemove     )
        .flatMap( part => Stream(part.p1, part.p2)).groupBy(grp =>Tuple2(grp.x , grp.y)).filter(_._2.size == 1).map(_._2(0)).toList
      assert(takenPoints.size == 2)

      // form 2 new parts and place is it into the structure
      this.partList = this.partList ::: List(Part(takenPoints(0), point, takenPoints(1)))
      this.partList = this.partList ::: List(Part(takenPoints(1), point, takenPoints(0)))

    }

    private def add(newPart: Part) = {
      partList = partList ::: List(newPart)
    }
  }

  case class Point(x: Double, y: Double) {
    def distance(point: Point): Double = Solution.this.distance(this, point)
  }

  case class Part(p1: Point, p2: Point, pointForNVec: Point) {
    // how to have line containing these points def:  y = ax+b  ===> Ax+By+C=0
    var line: Line = {

      // for the Line model : Ax+By+C = 0
      // Initial equation:
      // (y - p1.y)(p2.x - p1.x) - (x - p1.x)(p2.y - p1.y) = 0
      // y(p2.x - p1.x) - p1.y(p2.x - p1.x) - x(p2.y - p1.y) + p1.x(p2.y - p1.y) = 0
      // x(-p2.y + p1.y)  + y(p2.x - p1.x) + p1.x(p2.y - p1.y)  - p1.y(p2.x - p1.x)  = 0
      // A = p1.y - p2.y
      // B = p2.x - p1.x
      // C = p1.x(p2.y - p1.y)  - p1.y(p2.x - p1.x)
      val A = p1.y - p2.y
      val B = p2.x - p1.x
      val C = p1.x * (p2.y - p1.y) - p1.y * (p2.x - p1.x)
      Line(A, B, C)
    }

    // own normal vector:
    val normalVector: Vec = normalizedVectorToPoint(pointForNVec)

    def size = distance(p1, p2)

    // normal vector of part using given point
    // if point is already on the part.line - zeroVector is returned
    def normalizedVectorToPoint(point: Point): Vec = {

      if (this.line.goThrough(point)) {
        debug("-- part line : " + this + " is going through point  " + point + " -> zeroVector !")
        zeroVector
      } else {
        // get the perpendicular line to line going through consided part
        val perpendicular = this.line.getPerpendicular(point)

        val castPoint = findLineCross(this.line.A, this.line.B, this.line.C, perpendicular.A, perpendicular.B, perpendicular.C)

        return createNormalizedVector(castPoint, point)
      }
    }
  }

  def findLineCross(A1: Double, B1: Double, C1: Double, A2: Double, B2: Double, C2: Double) = {
    // get the cross point of these 2 lines :
    // metoda wyznacznikow
    val W = A1 * B2 - A2 * B1
    val Wx = (-C1) * B2 - (-C2) * B1
    val Wy = A1 * (-C2) - A2 * (-C1)
    assert(W != 0, "W shouldnt be zero (this method should be used only for non-parallel lines)")
    val x = Wx / W
    val y = Wy / W
    Point(x, y)
  }

  case class Line(A: Double, B: Double, C: Double) {
    def goThrough(point: Point): Boolean = (A * point.x + B * point.y + C == 0)

    def getPerpendicular(point: Point): Line = {
      //        Prostopadla do Ax+By+C = 0
      //        to A2=-B, B2=A , a C mozna wziac rozwiazujac dla podanego punktu
      val perpendicularA: Double = -B;
      val perpendicularB: Double = A;
      val perpendicularC: Double = -perpendicularA * point.x - perpendicularB * point.y;

      Line(perpendicularA, perpendicularB, perpendicularC)

    }
  }

  case class Vec(zero: Point, end: Point)

  def debug(obj:Any) = {
    //println(obj)
  }

  val finalAccuracy = 0.2
  val accuracy: Double = 0.00001
  val zeroVector = Vec(Point(0, 0), Point(0, 0))

}
