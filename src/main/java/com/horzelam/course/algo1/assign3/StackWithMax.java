package com.horzelam.course.algo1.assign3;

public class StackWithMax {

    Node top;


    class Node {
        Node prev;
        Integer val;

        public Node(final Integer val, final Node lastTop) {
            this.val = val;
            this.prev = lastTop;
        }
    }

    private Integer max() {
        return null;
    }

    public void push(Integer val) {
        final Node lastTop = top;
        top = new Node(val, lastTop);
    }

    public Integer pop() {
        final Node lastTop = top;
        if (lastTop != null) {
            this.top = lastTop.prev;
            return lastTop.val;
        } else {
            return null;
        }
    }

    public static void main(String[] args) {
        final StackWithMax stack = new StackWithMax();
        stack.push(100);
        stack.push(22);
        System.out.println(stack.pop());
        stack.push(23);
        stack.push(24);
        System.out.println("max is: " + stack.max());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());

    }

}
