package com.horzelam.course.algo1.assign3;

import java.util.Stack;

public class QueueBasedOn2Stacks<T> {
    Stack<T> inStack = new Stack();
    Stack<T> outStack = new Stack();

    public static void main(String[] args) {
        final QueueBasedOn2Stacks<String> queue = new QueueBasedOn2Stacks();
        queue.add("test 1");
        queue.add("test 2");
        queue.add("test 3");
        System.out.println(queue.remove());
        queue.add("test 4");
        System.out.println(queue.remove());
        queue.add("test 5");
        queue.add("test 6");
        queue.add("test 7");
        System.out.println(queue.remove());
        System.out.println(queue.remove());
        System.out.println(queue.remove());
        System.out.println(queue.remove());
        System.out.println(queue.remove());
        System.out.println(queue.remove());

    }

    public void add(T elem) {
        this.inStack.add(elem);
    }

    public T remove() {
        if (this.outStack.isEmpty()) {
            refillOut();
        }
        if (this.outStack.isEmpty()) {
            return null;
        } else {
            return this.outStack.pop();
        }
    }

    private void refillOut() {
        while (!this.inStack.isEmpty()) {
            this.outStack.push(this.inStack.pop());
        }
    }
}
