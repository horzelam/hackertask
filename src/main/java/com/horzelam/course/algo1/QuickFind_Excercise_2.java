package com.horzelam.course.algo1;

/**
 Question 2
 Union-find with specific canonical element.
 Add a method find() to the union-find data type so that
 find(i) returns the largest element in the connected component containing i.
 The operations, union(), connected(), and find() should all take logarithmic time or better.

 For example, if one of the connected components is {1, 2, 6, 9},
 then the find() method should return 9 for each of the four elements in the connected components.
 ---
 Hint: maintain an extra array to the weighted quick-union data structure
 that stores for each root i the large element in the connected component containing i
 */
public class QuickFind_Excercise_2 {

    Integer[] roots;
    Integer[] rootLarge;
    Integer[] rootSizes;

    public static void main(String[] args) {
        final QuickFind_Excercise_2 snc = new QuickFind_Excercise_2(10);
        snc.union(1, 2);
        snc.union(2, 6);
        snc.union(6, 9);
        snc.union(6, 5);
        snc.debug();
        System.out.println("1 is in subtree with large:" + snc.findLarge(1));
        System.out.println("2 is in subtree with large:" + snc.findLarge(2));
        System.out.println("6 is in subtree with large:" + snc.findLarge(6));
        System.out.println("9 is in subtree with large:" + snc.findLarge(9));
        System.out.println("5 is in subtree with large:" + snc.findLarge(5));
    }

    private void debug() {
        System.out.println("roots:");
        for (int i = 0; i < roots.length; i++) {
            System.out.printf(roots[i] + ",");
        }
        System.out.println("\nrootLarge:");
        for (int i = 0; i < rootLarge.length; i++) {
            System.out.printf(rootLarge[i] + ",");
        }
        System.out.println("");
    }

    public QuickFind_Excercise_2(final int elementsSize) {
        this.roots = new Integer[elementsSize];
        this.rootSizes = new Integer[elementsSize];
        this.rootLarge = new Integer[elementsSize];
    }


    int findLarge(int nodeId) {
        final int root = find(nodeId);
        return rootLarge[root];
    }

    private int find(int nodeId) {
        if (nodeId != roots[nodeId]) {
            roots[nodeId] = find(roots[nodeId]);
        }
        return roots[nodeId];
    }
    //    boolean find(int a, int b) {
//        return roots[a] == roots[b];
//    }

    /**
     * Unions two elements and returns true when all elements are already connected
     */
    boolean union(int a, int b) {
        int currentRootSize;
        int root;
        if (roots[a] == null && roots[b] == null) {
            root=a;
            roots[a] = root;
            roots[b] = root;
            rootSizes[a] = 2;
            rootSizes[b] = 1;
            rootLarge[a] = calculateLarger(a,b);
            rootLarge[b] = rootLarge[a];
            //System.out.println(" .. both new");
        } else if (roots[a] == null && roots[b] != null) {
            root = find(b);
            roots[a] = b;
            rootSizes[root] += 1;
            // update larger
            rootLarge[root] = calculateLarger(a,rootLarge[root]);
            // TODO - rest of sub-structure is not update - but either way findLarge() will go up
            //            rootLarge[a] = rootLarge[root];
            //            rootLarge[b] = rootLarge[root];
            // ..
            //System.out.println(" .. A new");
        } else if (roots[a] != null && roots[b] == null) {
            root = find(a);
            roots[b] = a;
            rootSizes[root] += 1;
            rootLarge[root] = calculateLarger(b,rootLarge[root]);
            //System.out.println(" .. B new");
        } else {
            // choose smaller tree and join under bigger
            int rootA = find(a);
            int rootB = find(b);
            if (rootA == rootB) {
                root = rootA;
                //System.out.println(" .. " + a + " and " + b + " - the same root");
            } else if (rootSizes[rootA] > rootSizes[rootB]) {
                root = rootA;
                System.out.println(" .. " + a + " has Bigger subtree than " + b);
                mergeSubTrees(rootA, rootB);

            } else {
                root = rootB;
                System.out.println(" .. " + a + " has Smaller subtree than " + b);
                mergeSubTrees(rootB, rootA);
            }
        }
        currentRootSize = rootSizes[root];
        String state = " - join on " + a + "," + b + " size is : " + currentRootSize;
        if (currentRootSize == this.roots.length) {
            System.out.println("Found complete connected - " + state);
            return true;
        } else {
            System.out.println("..Not yet complete connected - " + state);
            return false;
        }

    }

    private int calculateLarger(final int a, final int b) {
        return a >= b ? a : b;
    }

    private void mergeSubTrees(final int biggerSubTreeRoot, final int smallerSubTreeRoot) {
        roots[smallerSubTreeRoot] = biggerSubTreeRoot;
        rootSizes[biggerSubTreeRoot] += rootSizes[smallerSubTreeRoot];
        // both roots should already have entries in rootLarge
        rootLarge[biggerSubTreeRoot] = calculateLarger(rootLarge[biggerSubTreeRoot],rootLarge[smallerSubTreeRoot]);
    }

}
