package com.horzelam.course.algo1;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Question 3
 * Successor with delete. Given a set of n integers S = { 0, 1, ... , n-1 }
 * and a sequence of requests of the following form:
 * - Remove x from S
 * - Find the successor of x: the smallest y in S such that y≥x.
 * design a data type so that
 * all operations (except construction)
 * take logarithmic time or better in the worst case.
 */
public class QuickFind_SuccessorWithDelete {

    private static final Integer HEAD = -1;
    private static final Integer EMPTY = null;
    private Integer[] leftnodepointer;
    private Integer[] rightnodepointer;
    private Integer firstNodePointer;

    public static void main(String[] args) {
        final Set<Integer> input = new HashSet<>();
        input.addAll(Arrays.asList(0,1,2,3,4,5,6,7,8,9));
        final QuickFind_SuccessorWithDelete swd = new QuickFind_SuccessorWithDelete(input);

        swd.remove(4);
        System.out.println("Result for 4: " + swd.find(4));

        swd.remove(3);
        System.out.println("Result for 3: " + swd.find(3));

        swd.remove(5);
        System.out.println("Result for 5: " + swd.find(5));

        swd.remove(2);
        System.out.println("Result for 2: " + swd.find(2));

        swd.remove(1);
        System.out.println("Result for 2: " + swd.find(1));
        swd.debug();

    }

    private void debug() {
        System.out.println("leftnodepointer:  " + Arrays.toString(this.leftnodepointer));
        System.out.println("rightnodepointer:  " + Arrays.toString(this.rightnodepointer));
    }

    QuickFind_SuccessorWithDelete(Set<Integer> input) {

        // consturuct from Input Set -> internal array with linked elements ordered from lowest to biggest
        final Integer[] sorted = input.toArray(new Integer[input.size()]);
        Arrays.sort(sorted);
        leftnodepointer = new Integer[input.size()];
        rightnodepointer = new Integer[input.size()];

        Integer prevElement = HEAD;
        for (int i = 0; i < sorted.length; i++) {
            final int currElement = sorted[i];
            leftnodepointer[currElement] = prevElement;
            if (prevElement != HEAD) {
                rightnodepointer[prevElement] = currElement;
            } else {
                firstNodePointer = currElement;
            }
            prevElement = currElement;
        }
    }

    void remove(int x) {

        if (leftnodepointer[x] == EMPTY) {
            return;
        }

        final Integer leftNeighbourNode = leftnodepointer[x];
        final Integer rightNeighbourNode = rightnodepointer[x];

        if(leftNeighbourNode == HEAD){
            firstNodePointer = rightNeighbourNode;
        } else {
            rightnodepointer[leftNeighbourNode] = rightNeighbourNode;
        }
        leftnodepointer[rightNeighbourNode] = leftNeighbourNode;

        leftnodepointer[x] = EMPTY;
        rightnodepointer[x] = EMPTY;

    }

    // Find the successor of x: the smallest y in S such that y≥x.
    // Current O(n) = n in worst case
    private Integer find(final int x) {
        Integer idx = firstNodePointer;
        // find x
        while( idx < x ){
            idx = rightnodepointer[idx];
        }
        return idx;
    }
}
