package com.horzelam.course.algo1;

public class QuickFind_SocialNetworkConnectivity {

    Integer[] roots;
    Integer[] rootSizes;

    public static void main(String[] args) {
        final QuickFind_SocialNetworkConnectivity snc = new QuickFind_SocialNetworkConnectivity(10);
        snc.union(0, 1);
        snc.union(9, 5);
        snc.union(0, 2);
        snc.debug();

        snc.union(2, 9);
        snc.union(9, 1);
        snc.debug();

        snc.union(0, 3);
        snc.union(0, 4);
        snc.union(8, 2);
        snc.union(5, 6);
        // this should be the moment when all are connected !
        snc.debug();
        if (snc.union(9, 7) != true) {
            throw new IllegalStateException("Expecting this to finish with true");
        }
        snc.debug();

        snc.union(9, 8);
        snc.union(9, 2);
    }

    private void debug() {
        System.out.println("");
        for (int i = 0; i < roots.length; i++) {
            System.out.printf(roots[i] + ",");
        }
        System.out.println("");
    }

    public QuickFind_SocialNetworkConnectivity(final int elementsSize) {
        this.roots = new Integer[elementsSize];
        this.rootSizes = new Integer[elementsSize];
    }

    boolean find(int a, int b) {
        return roots[a] == roots[b];
    }

    /**
     * Unions two elements and returns true when all elements are already connected
     */
    boolean union(int a, int b) {
        int currentRootSize;
        int root;
        if (roots[a] == null && roots[b] == null) {
            root=a;
            roots[a] = root;
            roots[b] = root;
            rootSizes[a] = 2;
            rootSizes[b] = 1;
            //System.out.println(" .. both new");
        } else if (roots[a] == null && roots[b] != null) {
            root = find(b);
            roots[a] = b;
            rootSizes[root] += 1;
            //System.out.println(" .. A new");
        } else if (roots[a] != null && roots[b] == null) {
            root = find(a);
            roots[b] = a;
            rootSizes[root] += 1;
            //System.out.println(" .. B new");
        } else {
            // choose smaller tree and join under bigger
            int rootA = find(a);
            int rootB = find(b);
            if (rootA == rootB) {
                root = rootA;
                //System.out.println(" .. " + a + " and " + b + " - the same root");
            } else if (rootSizes[rootA] > rootSizes[rootB]) {
                root = rootA;
                System.out.println(" .. " + a + " has Bigger subtree than " + b);
                mergeSubTrees(rootA, rootB);

            } else {
                root = rootB;
                System.out.println(" .. " + a + " has Smaller subtree than " + b);
                mergeSubTrees(rootB, rootA);
            }
        }
        currentRootSize = rootSizes[root];
        String state = " - join on " + a + "," + b + " size is : " + currentRootSize;
        if (currentRootSize == this.roots.length) {
            System.out.println("Found complete connected - " + state);
            return true;
        } else {
            System.out.println("..Not yet complete connected - " + state);
            return false;
        }

    }

    private void mergeSubTrees(final int biggerSubTreeRoot, final int smallerSubTreeRoot) {
        roots[smallerSubTreeRoot] = biggerSubTreeRoot;
        rootSizes[biggerSubTreeRoot] += rootSizes[smallerSubTreeRoot];
    }

    private int find(int nodeId) {
        while (nodeId != roots[nodeId]) {
            //roots[nodeId] = roots[parent];
            nodeId = roots[nodeId];
        }
        return nodeId;
    }
}
