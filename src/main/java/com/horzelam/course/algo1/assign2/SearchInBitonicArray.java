package com.horzelam.course.algo1.assign2;

public class SearchInBitonicArray {
    public static void main(String[] args) {

        // given
        final int inputVal = 99;
        final int[] array = new int[]{1, 2, 7, 5, 4, 3, 0, -1, -100};
        //final int[] array = new int[]{1, 2, 7, 5, 4, 3, 0};

        final int n = array.length;

        //solution
        // 1. find a highest point - ln(N)
        int left = 0;
        int right = n - 1;
        int downMaxId = 0;
        int downMinId = right;
        int upMaxId = right;
        int upMinId = 0;
        while (left < right - 1) {
            int pivot = left + (right - left) / 2;
            if (array[pivot - 1] >= array[pivot]) {
                // we are on down slope - pivot is behind the highest value
                right = pivot;
                System.out.println(".. pivot on down slope with pivot:" + pivot);
                if (array[pivot] > inputVal && downMaxId == 0) {
                    downMaxId = pivot;
                } else if (array[pivot] < inputVal) {
                    downMinId = pivot;
                }
            } else {
                // we are on up slope
                left = pivot;
                System.out.println(".. pivot on up slope with pivot:" + pivot);
                if (array[pivot] > inputVal && upMaxId == n - 1) {
                    System.out.println("Changing upMax to " + pivot);
                    upMaxId = pivot;
                } else if (array[pivot] < inputVal) {
                    System.out.println("Changing upMin to " + pivot);
                    upMinId = pivot;
                }
            }
            System.out.println(" iteration: < " + left + " " + right + " >");
        }
        int highestIdx = array[left] > array[right] ? left : right;
        downMaxId = Math.max(highestIdx, downMaxId);
        upMaxId = Math.max(highestIdx, upMaxId);

        System.out.println("found pivot at : " + highestIdx + " - value: " + array[highestIdx]);
        System.out.println("on up slope (left one) will search in idx range: " + upMinId + ".. " + upMaxId);
        System.out.println("on down slope (right one) will search in idx range: " + downMaxId + ".. " + downMinId);

        if (array[highestIdx] < inputVal) {
            System.out.println("!! RESULT: There is no such val !");
        } else if (array[highestIdx] == inputVal) {
            System.out.println("!! RESULT: Val is found at id: " + highestIdx);
        } else {
            int foundA = findIntInArray(array, inputVal, upMinId, upMaxId);
            if (foundA >= 0) {
                System.out.println("!! RESULT: Found in Up array at idx: " + foundA);
            }
            int foundB = findIntInArray(array, inputVal, downMaxId, downMinId);
            if (foundB >= 0) {
                System.out.println("!! RESULT: Found in Down array at idx:" + foundB);
            }
            if (foundA == -1 && foundB == -1) {
                System.out.println("!! RESULT: There is no such val !");
            }
        }

    }

    private static int findIntInArray(final int[] array, final int inputVal, final int start, final int end) {
        int left = start;
        int right = end;
        boolean isUpArray = array[start] < array[end];
        while (left < right - 1) {
            int pivot = left + (right - left) / 2;
            if (inputVal == array[pivot]) {
                // we are on down slope - pivot is behind the highest value
                return pivot;
            } else if (array[pivot] < inputVal) {
                if (isUpArray) {
                    left = pivot;
                } else {
                    right = pivot;
                }
            } else {
                // array[pivot] is > inputVal
                if (isUpArray) {
                    right = pivot;
                } else {
                    left = pivot;
                }
            }
        }
        if (array[right] == inputVal) {
            return right;
        } else if (array[left] == inputVal) {
            return left;
        } else {
            return -1;
        }
    }

}
