package com.horzelam.course.algo1.assign2;

public class ThreeSumInQuadTime {
    public static void main(String[] args) {

        // given
        final int[] array = new int[]{1,2,3,4,5,6,7,8,9,10};
        final int sum = 11;

        // solution

        for (int i = 0; i < array.length; i++) {
            int rest = sum - array[i];
            int left = 0, right = array.length - 1; //two pointers
            while (left < right) {
                if (array[left] + array[right] == rest) {
                    break;
                } else if (array[left] + array[right] > rest) {
                    right--;
                } else {
                    left++;
                }
            }
            if(left < right){
                System.out.println("Found indexes: " + i + " " + left + " " + right);
                System.out.println("......values: " + array[i] + " " + array[left] + " " + array[right]);
            }
            else {
                System.out.println("Not found ");
            }
        }

    }
}
