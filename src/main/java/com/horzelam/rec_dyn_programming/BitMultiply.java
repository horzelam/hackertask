package com.horzelam.rec_dyn_programming;

public class BitMultiply {
    public static void main(String[] args) {

        // input numbers to multiply:
        int a = 13, m = 100;
        int result = 0;
        for (int k = 0; m > 0; k++) {
            if ((m & 1) == 1) {
                result += a << k;
            }
            m >>=1;
        }
        System.out.println("Multiply result is : " + result);
    }
}
