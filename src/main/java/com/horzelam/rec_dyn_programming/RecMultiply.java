package com.horzelam.rec_dyn_programming;

public class RecMultiply {
    public static void main(String[] args) {
        System.out.println(f(4, 9));
        System.out.println(f(13, 100));
        System.out.println(f(0, 0));
        System.out.println(f(0, 1));
        System.out.println(f(1, 0));
        System.out.println(f(1, 1));
        System.out.println(f(Integer.MAX_VALUE / 10, 9));
    }

    private static int f(final int a, final int b) {
        final int smaller = a < b ? a : b;
        final int bigger = a < b ? b : a;
        return fhelper(smaller, bigger);
    }

    private static int fhelper(final int smaller, final int bigger) {
        if (smaller == 0 || bigger == 0) {
            return 0;
        }
        int sub = f(smaller, bigger >> 1);
        if ((bigger & 1) == 1) {
            return sub + sub + smaller;
        } else {
            return sub + sub;

        }
    }
}
