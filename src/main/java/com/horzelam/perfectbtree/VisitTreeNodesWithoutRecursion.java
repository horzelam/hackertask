package com.horzelam.perfectbtree;

import java.util.Stack;

public class VisitTreeNodesWithoutRecursion {

    private static class TreeNode {
        int data;
        TreeNode left;
        TreeNode right;

        public TreeNode(final int data, final TreeNode l, final TreeNode right) {
            this.data = data;
            this.left = l;
            this.right = right;
        }
    }

    //Read more:https://www.java67.com/2016/07/binary-tree-preorder-traversal-in-java-without-recursion.html#ixzz6SNzJq6DK
    public static void main(String[] args) {

        TreeNode root = new TreeNode(1, new TreeNode(2, null, new TreeNode(4, null, null)),
                new TreeNode(3,
                        new TreeNode(5, new TreeNode(7, null, null), new TreeNode(8, null, null)),
                        new TreeNode(6, new TreeNode(9, null, null),
                                new TreeNode(10, new TreeNode(11, null, null), null))));
        preOrderWithoutRecursion(root);
    }

    public static void preOrderWithoutRecursion(final TreeNode root) {
        Stack<TreeNode> nodes = new Stack<>();
        nodes.push(root);
        while (!nodes.isEmpty()) {
            TreeNode current = nodes.pop();
            System.out.printf("%s ", current.data);
            if (current.right != null) {
                nodes.push(current.right);
            }
            if (current.left != null) {
                nodes.push(current.left);
            }
        }
    }

}
