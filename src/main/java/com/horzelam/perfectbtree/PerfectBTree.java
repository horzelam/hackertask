package com.horzelam.perfectbtree;

public class PerfectBTree {
    // Finds the biggest perfect subtree in tree
    // Note : it's not perfect solution - because its simple recursion - will fail probably before reaching 100.000 levels !!!


    private final static Tree None = null;

    public static void main(String[] args) {
        final Tree tree1 = new Tree(1, new Tree(2, None, new Tree(4, None, None)),
                new Tree(3, new Tree(5, new Tree(7, None, None), new Tree(8, None, None)),
                        new Tree(6, new Tree(9, None, None), new Tree(10, new Tree(11, None, None), None))));
        final Tree tree2 = new Tree(1, new Tree(13, new Tree(15, new Tree(17, None, None), new Tree(18, None, None)),
                new Tree(16, new Tree(19, None, None), new Tree(20, new Tree(21, None, None), new Tree(31, None, None)))),
                new Tree(3, new Tree(5, new Tree(7, None, None), new Tree(8, None, None)),
                        new Tree(6, new Tree(9, None, None), new Tree(10, new Tree(11, None, None), new Tree(32, None, None)))));

        final Tree tree3 = new Tree(1, new Tree(2, None, new Tree(4, None, None)),
                new Tree(3, new Tree(5, new Tree(7, new Tree(21, None, None), None), new Tree(8, None, None)),
                        new Tree(6, new Tree(9, None, None), new Tree(10, new Tree(11, None, None), None))));

        final Tree tree4 = new Tree(1, new Tree(2, None, new Tree(4, None, None)), new Tree(3,
                new Tree(5, new Tree(7, new Tree(21, None, None), new Tree(22, None, None)),
                        new Tree(8, new Tree(23, None, None), new Tree(24, None, None))),
                new Tree(6, new Tree(9, new Tree(25, None, None), new Tree(26, None, None)),
                        new Tree(10, new Tree(27, None, None), new Tree(28, None, None)))));
        System.out.println("result: " + solution(tree1) + " expected: " + 7);
        System.out.println("result: " + solution(tree2) + " expected: " + 15);
        System.out.println("result: " + solution(tree3) + " expected: " + 7);
        System.out.println("result: " + solution(tree4) + " expected: " + 15);
    }

    public static int solution(Tree t) {
        if (t == None) {
            return 0;
        } else if (t.l == None || t.r == None) {
            return 1;
        } else {
            int lsize = solution(t.l);
            int rsize = solution(t.r);
            if (lsize == rsize) {
                //System.out.println("..2 children " + t.x);
                return lsize + rsize + 1;
            } else {
                //System.out.println("..2 uneq children " + t.x);
                return lsize > rsize ? lsize : rsize;
            }
        }
    }

    private static class Tree {
        int x;
        Tree l;
        Tree r;

        public Tree(final int x, final Tree l, final Tree r) {
            this.x = x;
            this.l = l;
            this.r = r;
        }
    }
}
