package com.horzelam.camelCase

/**
  * Created by osboxes on 07/01/17.
  */
object Solution {

  def solve(s: String):Int =
    s.filter(char => char.isUpper).size + 1;

  def main(args: Array[String]) {
    //val sc = new java.util.Scanner (System.in);
    //var s = sc.next();

    println(solve("theWorldOfFour"))
    println(solve("theWorldOfFiveWords"))
  }

}
