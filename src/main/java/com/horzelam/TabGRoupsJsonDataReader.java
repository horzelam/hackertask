package com.horzelam;

/**
 * Created by morele on 2017-11-15.
 */
public class TabGRoupsJsonDataReader {
/*
    public static void main(String[] args) {
        System.out.println("Run..");

        String fileName = "C:\\Users\\morele\\Documents\\Michal\\ff-backup\\odzyskiwanie.json";
        JSONParser parser = new JSONParser();
        Map<Long, String> tabNames = prepareTabNames();

        Map<Long, List<String>> urlsGrouped = new HashMap<>();

        try {

            Object obj = parser.parse(new FileReader(fileName));

            JSONObject jsonObject = (JSONObject) obj;
            //System.out.println(jsonObject);
            JSONArray tabs = (JSONArray) ((JSONObject) ((JSONArray) jsonObject.get("windows")).get(0)).get("tabs");


            // loop array
            Iterator<Object> iterator = tabs.iterator();
            while (iterator.hasNext()) {
                JSONObject tab = (JSONObject) iterator.next();

                JSONObject extData = (JSONObject) (tab).get("extData");
                JSONArray entries = (JSONArray) (tab).get("entries");

                Object data = extData.get("tabview-tab");
                if (data != null) {
                    //Object grId = ((JSONObject) data).get("groupID");
                    JSONObject json = (JSONObject) new JSONParser().parse((String) data);
                    if (json != null && json.containsKey("groupID")) {
                        Long currentGroupId = (Long) json.get("groupID");
                        JSONObject lastEntry = (JSONObject) entries.get(entries.size() - 1);
                        System.out.println(currentGroupId + "  last entry : " + lastEntry.get("ID") + " - " +
                                lastEntry.get("url"));
                        List<String> listUpdated;
                        if(!urlsGrouped.containsKey(currentGroupId)){
                            listUpdated = new ArrayList<>();
                        }else {
                            listUpdated = urlsGrouped.get(currentGroupId);
                        }
                        listUpdated.add((String) lastEntry.get("url"));

                        urlsGrouped.put(currentGroupId, listUpdated);
                    }
                }
            }

            System.out.println("--------");
            urlsGrouped.forEach( (groupId,list)-> {
                System.out.println("Group : " + groupId + " - "  + tabNames.get(groupId));
                list.forEach(url -> System.out.println( " - url: " + url));

            });

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }


    }

    private static Map<Long, String> prepareTabNames() {
        HashMap<Long, String> tabs = new HashMap<Long, String>();
        tabs.put(2L, "Rozne");
        tabs.put(3L, "Mail");
        tabs.put(4L, "Audio");
        tabs.put(5L, "Painting");
        tabs.put(6L, "Dev");
        tabs.put(7L, "Pracai organizacja");
        tabs.put(8L, "Raspberry");
        tabs.put(9L, "Scianaobrazkow");
        tabs.put(10L, "gamedev");
        tabs.put(11L, "urbanwatercolour");
        tabs.put(12L, "Berlin-mieszkania");
        tabs.put(13L, "PaintingPortraits");
        tabs.put(14L, "Ink");
        tabs.put(15L, "watercolour");
        tabs.put(16L, "FigureInsp");
        tabs.put(17L, "Figure2");
        tabs.put(19L, "Figure3");
        tabs.put(21L, "BuyArt  Equip");
        return tabs;
    }

 */
}
