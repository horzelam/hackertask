package com.horzelam.find_pair_in_array_giving_sum;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

// Write a program that, given an array A[] of n numbers and another number x,
// determines whether or not there exist two elements in S whose sum is exactly x.
// see:
// 1. https://www.geeksforgeeks.org/given-an-array-a-and-a-number-x-check-for-pair-in-a-with-sum-as-x/
// 2. https://algopro.in/questions/category/array/pair-that-adds-up-to-given-number
public class FindPairGivingSum {
    public static void main(String[] args) {

        // METHOD 1
        System.out.println(
                "Expected: " + Arrays.toString(new int[]{-3, 1}) + ", found:" + Arrays.toString(find(new int[]{0, -1, 2, -3, 1}, -2)));

        System.out.println(
                "Expected: " + Arrays.toString(new int[]{-1}) + ", found:" + Arrays.toString(find(new int[]{1, -2, 1, 0, 5}, 0)));

        // METHOD 2 - better - hashing - O(n)

        System.out.println(
                "Expected: " + Arrays.toString(new int[]{-3, 1}) + ", found:" + Arrays.toString(findUsingHasing(new int[]{0, -1, 2, -3, 1}, -2)));

        System.out.println(
                "Expected: " + Arrays.toString(new int[]{-1}) + ", found:" + Arrays.toString(findUsingHasing(new int[]{1, -2, 1, 0, 5}, 0)));

    }

    private static int[] find(final int[] array, final int expectedSum) {
        // method 1 : Sorting and Two-Pointers technique.

        // complexity - depending on sorting
        // - quicksort -n^2
        // - Merge Sort or Heap Sort is used then (-)(nlogn) in worst case.
        Arrays.sort(array);

        int left = 0;
        int right = array.length - 1;

        while (left < right) {
            int currentSum = array[left] + array[right];
            if (currentSum == expectedSum) {
                return new int[]{array[left], array[right]};
                //found candidates
            } else if (currentSum < expectedSum) {
                // shift  up
                left++;
            } else {
                // shift down
                right--;
            }
        }
        return new int[]{-1};
    }

    // BETTER ALGO - based on Hasing
    // worst case time complexity: O(n) - in case we have sum of 1st and last element,
    //          but we trade it for space complexity
    // worst case space complexity : O(n) - because it might be all elements will go to extra map
    // see bottom of : https://www.geeksforgeeks.org/given-an-array-a-and-a-number-x-check-for-pair-in-a-with-sum-as-x/

    private static int[] findUsingHasing(final int[] array, final int expectedSum) {
        Set<Integer> s = new HashSet<Integer>();
        for (int i = 0; i < array.length; ++i) {
            int temp = expectedSum - array[i];

            // checking for condition
            if (s.contains(temp)) {
                return new int[]{temp, array[i]};
            }
            s.add(array[i]);
        }

        return new int[]{-1};
    }

}
