package com.horzelam.codility;

import java.util.Arrays;

public class MissingInt {

    public static void main(String[] args) {
        int result = new MissingInt().solution(new int[]{-1,-2, 4, 5, 3, 1, 0, -1});
        System.out.println(result);
    }

    public int solution(int[] A) {
        int result = 1;
        // write your code in Java SE 8

        Arrays.sort(A);
        for (int i = 0; i < A.length; i++){
            if(A[i]==result){
                result++;
            }
            if(A[i]>result){
                break;
            }
        }
        return result;
    }
}
