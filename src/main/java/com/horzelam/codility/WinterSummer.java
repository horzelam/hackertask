package com.horzelam.codility;


import java.util.*;

public class WinterSummer {


    public static void main(String[] args) {
        int result = new WinterSummer().solution(new int[]{-5, 3, -5, -42, 6, 5, 3, 6,12});
        System.out.println(result);
    }

    public int solution(int[] T) {


        int minIdx = 0;
        int min = T[0];

        int maxTillMinIdx = 0;
        int maxTillMin = min;

        int maxTillCurrentIdx = 0;
        int maxTillCurrent = min;

        int maxTillMinCandidateIdx = 0;
        int maxTillMinCandidate = min;

        int possibleSummerStartIdx = 0;

        for (int i = 1; i < T.length; i++) {
            if(T[i]<=min) {
                min = T[i];
                minIdx = i;
                maxTillMin = maxTillMinCandidate;
                maxTillMinIdx = maxTillMinCandidateIdx;

            }
            if (T[i] >= maxTillMin) {
                maxTillMinCandidate = T[i];
                maxTillMinCandidateIdx = i;
            }

            if(T[i] > maxTillCurrent){
                maxTillCurrentIdx = i;
                maxTillCurrent = T[i];
            }

            if(T[i] <= maxTillMin){
                possibleSummerStartIdx = i + 1;
            } else{
                // temp is higer than max temp till minumum temp
//                if(T[i] <= maxTillCurrent){
//                    possibleSummerStartIdx = i;
//                }
            }

        }
        System.out.println("Found min : " + min + " at " + minIdx );
        System.out.println("Found max till min : " + maxTillMin + " at " + maxTillMinIdx);

        System.out.println("Found smmmer start : " + T[possibleSummerStartIdx] + " at " + possibleSummerStartIdx);

        return 0;
    }
}
