package com.horzelam.codility;


import java.util.*;

public class Files {

    public enum FileType{
        music("mp3","aac","flac"), images("jpg","bmp","gif"), movies("mp4","avi","mkv"), other(null);
        private List<String> extensions = new ArrayList<>();

        private FileType(String... extensions){
            if(extensions!=null) {
                this.extensions = Arrays.asList(extensions);
            }
        }
        public static FileType detectType(String extension){
            return Arrays.stream(FileType.values()).filter(fileType -> fileType
                    .extensions.contains(extension)).findFirst().orElse(other);
        }
    }

    public static void main(String[] args) {
        StringBuilder input = new StringBuilder("");
        input.append("my.song.mp3 11b\n" +
                "greatSong.flac 1000b\n" +
                "not3.txt 5b\n" +
                "video.mp4 200b\n" +
                "game.exe 100b\n" +
                "mov!e.mkv 10000b");
        String result = new Files().solution(input.toString());
        System.out.println(result);
    }

    public String solution(String S) {

        Map<FileType, Integer> resultMap = new HashMap<>();
        String[] splitInput = S.split("\n");
        Arrays.stream(FileType.values()).forEach(type->{
            resultMap.put(type,0);
        });
        for (int i = 0; i < splitInput.length; i++){
            String fileLine = splitInput[i];
            System.out.println("file:" + splitInput[i]+"|");
            String[] fileAndSize = fileLine.split(" ");
            String fullName = fileAndSize[0];
            int size = Integer.parseInt(fileAndSize[1].substring(0, fileAndSize[1].length()-1));
            String extension = fullName .substring(fullName.lastIndexOf(".")+1);
            System.out.println(" - " + fullName);
            System.out.println(" - " + size);
            System.out.println(" - " + extension);

            FileType fileType = FileType.detectType(extension);
            int totalPerType = resultMap.get(fileType) + size;
            resultMap.put(fileType, totalPerType);
        }

        final StringBuilder result = new StringBuilder();

        Arrays.stream(FileType.values()).forEach(type->{
            if(result.length()>0){
                result.append("\n");
            }
            result.append(type.name() + " " + resultMap.get(type)+"b");

        });
        return result.toString();
    }
}
