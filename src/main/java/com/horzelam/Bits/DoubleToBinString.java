package com.horzelam.Bits;

import sun.misc.FloatingDecimal;

public class DoubleToBinString {
    public static void main(String[] args) {
        double var1 = 0.72d;
        double var2 = var1;

        final long l = Double.doubleToLongBits(var1);
        String bin = Long.toBinaryString(l);
        System.out.println(bin);
        System.out.println(bin.length());
        StringBuilder res0 = new StringBuilder();
        for (int i = 0; i < 64 && var1 >0; i++) {
            var1 = var1 * 2;
            final int resBit = (int) var1;

            res0.append(resBit);
            var1 = var1 - resBit;
            //System.out.println(" - " + resBit + " curr var : " + var);
        }
        System.out.println("Result1: "+  res0.toString());

        StringBuilder res = new StringBuilder();
        while (var2 > 0) {
            //long mask = (1) << i ;
            //final long bit = var & mask;
            var2 = var2 * 2;
            if (var2 >= 1) {
                res.append(1);
                var2 = var2 - 1;
            } else {
                res.append(0);
            }
        }
        System.out.println("Result2: " + res.toString());
        //        System.out.println(res.toString());
    }
}
