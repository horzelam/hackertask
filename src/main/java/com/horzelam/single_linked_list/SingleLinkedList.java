package com.horzelam.single_linked_list;

import java.util.Arrays;
import java.util.List;

public class SingleLinkedList {

    static final class Node {
        int value;
        Node next;

        public Node(final int value, final Node next) {
            this.value = value;
            this.next = next;
        }
    }

    public static void main(String[] args) {
        final List<Integer> list = Arrays.asList(1, 3, 7, 8, 9, 4, 5, 2, 1);
        final Node head = buildSingleLinkedList(list);
        System.out.println("List: " + debugList(head));
        final Node newHead = partition(5, head);
        System.out.println("List: " + debugList(newHead));
    }

    /**
     * Groups nodes with values < partitionValue to left and nodes with values >= partitionValue to right
     * (node with partitionValue can be in any place of right partition)
     * @return
     */
    private static Node partition(final int partitionValue, Node head) {
        Node newHead = head;
        Node node = head;
        while (node != null) {
            while(node.next != null && node.next.value < partitionValue) {
                Node nodeToShift = node.next;
                Node nodeAfterNext = node.next.next;
                nodeToShift.next = newHead;
                newHead = nodeToShift;
                node.next = nodeAfterNext;
            }
            node = node.next;
        }
        return newHead;
    }

    private static Node buildSingleLinkedList(final List<Integer> list) {
        Node head = null;
        Node prevNode = null;
        for (int val : list) {
            final Node node = new Node(val, null);
            if (prevNode == null) {
                head = node;
            } else {
                //if any node after first;
                prevNode.next = node;
            }
            prevNode = node;
        }
        return head;
    }

    private static String debugList(final Node head) {
        final StringBuilder str = new StringBuilder();
        Node node = head;
        while (node != null) {
            if (str.length() != 0) {
                str.append(", ");
            }
            str.append(node.value);
            node = node.next;
        }
        return str.toString();
    }
}

