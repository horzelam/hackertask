package com.horzelam;

/**
 * Hello world!
 *
 */
public class NextPalindromApp
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        System.out.println("test for 1234567 -> " + getNextPalindrom(1234567) );
        System.out.println("test for 1234367 -> " + getNextPalindrom(1234367) );
        System.out.println("test for 1234311 -> " + getNextPalindrom(1234311) );
        System.out.println("test for 101 -> " + getNextPalindrom(101) );
        System.out.println("test for 123456 -> " + getNextPalindrom(123456) );

        System.out.println("test for 0 -> " + getNextPalindrom(0) );
        System.out.println("test for 1 -> " + getNextPalindrom(1));
        System.out.println("test for 9 -> " + getNextPalindrom(9));
        System.out.println("test for 11 -> " + getNextPalindrom(11));
        System.out.println("test for 22 -> " + getNextPalindrom(22));
        System.out.println("test for 88 -> " + getNextPalindrom(88));
        System.out.println("test for 97 -> " + getNextPalindrom(97));
        System.out.println("test for 99 -> " + getNextPalindrom(99));
        System.out.println("test for 999 -> " + getNextPalindrom(999));
        System.out.println("test for 999999 -> " + getNextPalindrom(999999));

    }


    public static int getNextPalindrom(int input) {

        if(input < 9){
            return input+1;
        } else if(input ==9)
        {
            return 11;
        }


        String inputAsString = new Integer(input).toString();
        int totalSize = inputAsString.length();

        int halfIndex = totalSize / 2;
        int halfIndexCeiling = (int) Math.ceil( (float) totalSize / 2);



        String left = inputAsString.substring(0, halfIndex);
        String center = inputAsString.substring(halfIndex, halfIndex + 1);
        String rightReversed = new StringBuilder(inputAsString.substring(halfIndexCeiling)).reverse().toString();
//
//        System.out.println(" left: " + left);
//        System.out.println(" rightRev: " + rightReversed);
//        System.out.println(" center: " + center);
//        System.out.println(" halfIndex: " + halfIndex);
//        System.out.println(" halfIndexCeiling: " + halfIndexCeiling);

        Integer leftAsNumber = Integer.parseInt(left);
        Integer centerAsNumber = Integer.parseInt(center);
        Integer rightReversedAsNumber = Integer.parseInt(rightReversed);

        String resultLeftSide = "";
        String forRightSide = "";
        boolean oddSizeNumber  = halfIndex == halfIndexCeiling;
        if(leftAsNumber > rightReversedAsNumber){
            resultLeftSide = left + (oddSizeNumber?"":center);
            forRightSide = left;
        } else{
            if(oddSizeNumber){
                resultLeftSide = "" + (leftAsNumber+1);
                if(resultLeftSide.length() > left.length()){
                    forRightSide = resultLeftSide.substring(0,resultLeftSide.length()-1);
                }else {
                    forRightSide = resultLeftSide;
                }
            }else{
                resultLeftSide = "" + (leftAsNumber *10 + centerAsNumber+1 );
                if(resultLeftSide.length() > left.length()){
                    forRightSide =  resultLeftSide.substring(0,left.length());
                }else {
                    forRightSide = left;
                }
            }
        }
        String resultRightSideReversed = new StringBuilder(forRightSide).reverse().toString();
        String resultAsString = resultLeftSide +  resultRightSideReversed;
        return Integer.parseInt(resultAsString );


    }

    }
