package com.horzelam.tree;

public class Node<T> {
    T x;
    Node l;
    Node r;

    public Node(final T x, final Node l, final Node r) {
        this.x = x;
        this.l = l;
        this.r = r;
    }

    @Override
    public String toString() {
        return ""+x;
    }
}
