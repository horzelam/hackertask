package com.horzelam.tree;

/**
 * To find if bigger tree T1 contains smaller subtree T2
 */
public class CheckSubtree {

    private final static Node None = null;

    public static void main(String[] args) {
        final Node tree1 = new Node(1, new Node(2, None, new Node(4, None, None)),
                new Node(3, new Node(5, new Node(7, None, None), new Node(8, None, None)),
                        new Node(6, new Node(9, None, None), new Node(10, new Node(11, None, None), None))));
        final Node tree2 = new Node(6, new Node(9, None, None), new Node(10, new Node(11, None, None), None));
        BTreePrinter.printNode(tree1);
        BTreePrinter.printNode(tree2);

        System.out.println("Expected: true - result: " + checkSubtree1(tree1, tree2));

        final Node tree3 = new Node(1, new Node(1, None, new Node(1, None, None)),
                new Node(1, new Node(1, new Node(1, None, None), new Node(1, None, None)),
                        new Node(1, new Node(1, None, None), new Node(1, new Node(1, None, None), None))));

        final Node tree4 = new Node(1, new Node(1, None, None), new Node(1, new Node(1, None, None), None));
        final Node tree5 = new Node(1, new Node(1, None, None), new Node(1, new Node(1, None, new Node(1, None, None)), None));
        BTreePrinter.printNode(tree3);
        BTreePrinter.printNode(tree4);
        System.out.println("Expected: true - result: " + checkSubtree1(tree3, tree4));
        BTreePrinter.printNode(tree5);
        System.out.println("Expected: false - result: " + checkSubtree1(tree3, tree5));

        System.out.println("---------Algorithm 2:");
        System.out.println("Expected: true - result: "  + checkSubtree2(tree1, tree2));
        System.out.println("Expected: true - result: "  + checkSubtree2(tree3, tree4));
        System.out.println("Expected: false - result: "  + checkSubtree2(tree3, tree5));

    }

    /**
     * Alg 2 - Preorder to String
     * Checks if subtree t2 is contained in bigger t1
     */
    private static boolean checkSubtree2(final Node t1, final Node t2) {

        StringBuilder t1Str = new StringBuilder();
        StringBuilder t2Str = new StringBuilder();
        preOrderTraversalToString(t1, t1Str);
        System.out.println("String t1: " + t1Str.toString());
        preOrderTraversalToString(t2, t2Str);
        System.out.println("String t2: " + t2Str.toString());
        return t1Str.toString().contains(t2Str.toString());
    }

    private static void preOrderTraversalToString(final Node t, final StringBuilder str) {


        if(t != null) {
            str.append(t.x);
            preOrderTraversalToString(t.l, str);
            preOrderTraversalToString(t.r, str);
        }else {
            str.append("X");
        }

    }

    /**
     * Alg 1 - search & comparison
     * Checks if subtree t2 is contained in bigger t1
     */

    private static boolean checkSubtree1(final Node t1, final Node t2) {
        if(t2 == null) return true;
        return checkSubtree(t1,t2);
    }


    private static boolean checkSubtree(final Node t1, final Node t2) {
        // System.out.println("Checking : " + t1);
        // my bug:
        //        if (t1 == null || t2 == null) {
        //            return false;
        //        }
        if (t1 == null ) {
            return false;
        }
        else if (t1.x == t2.x && checkEqual(t1.l, t2.l) && checkEqual(t1.r, t2.r)) {
                return true;
        }
        return checkSubtree(t1.l, t2) || checkSubtree(t1.r, t2);
    }

    private static boolean checkEqual(final Node n, final Node t2) {
        if (n == null || t2 == null) {
            return n == t2;
        }
        if (n.x == t2.x) {
            //System.out.println("found equal " + n.x);
            return checkEqual(n.l, t2.l) && checkEqual(n.r, t2.r);
        } else {
            return false;
        }
    }

}
